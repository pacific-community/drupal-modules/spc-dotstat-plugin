<?php 
if( ! defined('WP_UNINSTALL_PLUGIN') ) exit;

// If validation compleate remove all options
delete_option('Activated_Dotstatdata');
delete_option('stat_chart_option_data');
flush_rewrite_rules();