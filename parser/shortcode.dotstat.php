<?php
require_once( plugin_dir_path( __FILE__ ) . 'parser.dotstat.php' );

add_shortcode( 'dotstat', 'dotstat_shortcode_data' );

function dotstat_shortcode_data( $atts ){

    global $post;

    if(isset($atts['display'])){
        $stat = intval($atts['display']);
        $post = get_post( $stat );
        if( ! $post || $post->post_type != 'dotstat' )
        return ''; 

        $shortcode = new renderDotstatShortcode($post);
        $shortcodeOutput = $shortcode->handleShortcode();        

        return $shortcodeOutput;

    } else {
      return "Not found";  
   }      
      
}



class renderDotstatShortcode {

  private $dotstat_post;
  private $depth;

  function __construct($post, $depth = 0) {

    $this->dotstat_post = $post;

    $datasetId = get_post_meta( $post->ID, 'chart_entity', true );
    $node = get_post($datasetId);
    $node_dataObj = json_decode(get_post_meta( $node->ID, 'tax_stored_parsed_data', true ), true);

    // Calculate depth of the data array
    $innerDepth = 0;    
    $iteIte = new RecursiveIteratorIterator(new RecursiveArrayIterator($node_dataObj['data']));
    foreach ($iteIte as $ite) {
      $d = $iteIte->getDepth();
      $innerDepth = $d > $innerDepth ? $d : $innerDepth;
    }
    $this->depth = $innerDepth > 0 && $innerDepth <= 3 ? $innerDepth - 1 : 0 ;
    // end Calculate depth of the data array
    

  }

  /**
   * check output type and generate html for needed type
   */
  public function handleShortcode(){

    $postId = $this->dotstat_post->ID;
    $type_display = get_post_meta( $postId, 'type_display', true );

    if($type_display){
      $type_val = strtolower(strval($type_display));
      
      switch ($type_val) {
        case 'table':
          return $this->renderTable();
          break;
        case 'chart':
          return $this->renderChart();
          break;
        case 'keystat':
          return $this->renderKeyStat();
          break;
      }
    }

    return '';
  }

  

  /**
   * Parse Table Value
   */
  protected function _parseValue($row, $cnf) {
    $arr = [
      'value' => 'n/c',
      'title' => ''
    ];

    switch ($cnf['dot_stat_table_format']) {
      case 1: // number
        if (is_float($row['value'])) {
          $decimals = ( (int) $row['value'] != $row['value'] ) ? (strlen($row['value']) - strpos($row['value'], '.')) - 1 : 0;
          $arr['value'] = number_format($row['value'], $decimals);
        } else if (is_integer($row['value'])) {
          $arr['value'] = number_format($row['value']);
        } else {
          $arr['value'] = 'n/c';
        }
        break;
      case 2: // percent
        $arr['value'] = $row['value'].'%';
        break;
      case 2: // rate
        $arr['value'] = (($row['value'] > 0)?'+':'').$row['value'].'%';
        break;
      case 4: // Yes/No
        $arr['value'] = ($row['value'] > 0)?'<span class="dot-stat-yes">Yes</span>':'<span class="dot-stat-no">No</span>';
        break;
      default:
        if (!empty($row['value'])) {
          $arr['value'] = $row['value'];
        }
        break;
    }

    if (isset($row['title'])) {
      $arr['title'] = $row['title'];
    }

    return $arr;
  }

  /**
   * @return string html markup of the table
   */
  private function renderTable() {

    $config = [];
    // $config['label'] = get_post_meta($this->dotstat_post->ID, 'table_label', true);
    $config['dot_stat_table_format'] = intval(get_post_meta($this->dotstat_post->ID, 'dot_stat_table_format', true));
    $config['dot_stat_table_column_name'] = get_post_meta($this->dotstat_post->ID, 'dot_stat_table_column_name', true);
    $config['dot_stat_table_header_css'] = get_post_meta($this->dotstat_post->ID, 'dot_stat_table_header_css', true);
    $config['dot_stat_table_title'] = get_post_meta($this->dotstat_post->ID, 'dot_stat_table_title', true);
    $config['dot_stat_table_link'] = get_post_meta($this->dotstat_post->ID, 'dot_stat_table_link', true);
        
    // _dbg('**** INIT build for '.$config['dot_stat_key_node']);
    $displayId = $this->dotstat_post->ID;
    $datasetId = get_post_meta( $displayId, 'chart_entity', true );
    $node = get_post($datasetId);
    $node_dataObj = json_decode(get_post_meta( $node->ID, 'tax_stored_parsed_data', true ), true);
    
    // $title = $config['label_display']?$config['label']:$node->label();
    // $title = $config['label'];
    
    // _dbg('> Data Stat : '.$title);

    // ---------- SERIES AND LABELS

    $series = $node_dataObj['series'];

    $rows = [
      'name'    => 'Items',
      'labels'  => [],
      'data'    => []
    ];

    if ($level1 = reset($series)) {

      $rows = [
        'name'    => $level1['name'],
        'labels'  => [],
        'data'    => []
      ];

      foreach ($level1['values'] as $obj) {
        $rows['labels'][ $obj['id'] ] = $obj['name'];
      }

    }

    // ---------- PARSE DATA

    // check depth
    $depth = $this->depth;

    // get data
    $data = $node_dataObj['data'];
    
    // start parsing
    $cols = array();

    switch ($depth) {

      case 0:
        // simple table (single column)
        $cols[0] = 'Data';
        foreach ($data as $k => $col) {
          $rows['labels'][$k] = $col['name'];
          $rows['data'][$k][0] = $this->_parseValue($col, $config);;
        }
        ksort($rows);
        break;

      case 1:
        // data series (multiple columns)
        foreach ($data as $k => $rowdata) {
          #('* '.$k.' ('.$config['dot_stat_table_format'].')');
          foreach ($rowdata as $colkey => $coldata) {
            if (!array_key_exists($colkey, $cols)) {
              #("  # new col $colkey: ".$coldata['name']);
              $cols[$colkey] = $coldata['name'];
            }
            $arv = $this->_parseValue($coldata, $config);
            #('  > '.$arv['value']);
            $rows['data'][$k][$colkey] = $arv;
          }

        }

    }
    
    // ---------- GENERATE HTML

    $align = 'text-align-left';
    switch ($config['dot_stat_table_format']) {
      case 1:
      case 2:
      case 3:
        $align = 'text-align-right';
        break;
      case 4:
        $align = 'text-align-center';
        break;
    }

    // --- ROWS

    $htmlrows = [];

    ksort($rows['data']);

    foreach ($rows['data'] as $key => $coldata) {
      $html = '<tr>'
        .'<th class="text-nowrap">'.$rows['labels'][$key].'</th>';
      foreach ($cols as $k => $n) {
        $tdv = '<td></td>';
        if (array_key_exists($k, $coldata)) {
          $arr = $coldata[$k];
          $tdv = '<td title="'.$arr['title'].'" data-toggle="tooltip" data-placement="top" class="'.$align.'">'.$arr['value'].'</td>';
        }
        $html .= $tdv;
      }
      $html .= '</tr>';
      $htmlrows[] = $html;
    }

    // --------------- LINK TO .STAT 
    
    $url = get_post_meta( $node->ID, 'default_link', true );

    if (!empty($config['dot_stat_table_link'])) {
      if ($config['dot_stat_table_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_table_link'];
      }
    }

       
    // --- TABLE & HEADERS
    
    $pf = '<div class="table-responsive">'
      .'<table class="stat-data-table table table-bordered table-striped table-hover table-xs '.$config['dot_stat_table_css'].'">';
    
    if (!empty($config['dot_stat_table_title'])) {
      $caption = $config['dot_stat_table_title'];
      if (!empty($url)) {
        $caption = '<a href="'.$url.'"';
        if (strpos($url, 'http') === 0) {
          $caption .= ' target="_blank"';
        }
        $caption .= '>'.$config['dot_stat_table_title'].'</a>';
      }
      $pf .= '<caption>'.$caption.'</caption>';
    }

    $headertitle = $rows['name'];
    $arrcoltitles = [];
    if (strpos($config['dot_stat_table_column_name'], '/') === false 
      && strpos($config['dot_stat_table_column_name'], '|') > 0) 
    {
      $arrcoltitles = explode('|', $config['dot_stat_table_column_name']);
      $headertitle = array_shift($arrcoltitles);
    }

    $pf .= '<thead class="thead-dark '.$config['dot_stat_table_header_css'].'"><tr>'
        .'<th class="text-align-left">'.$headertitle.'</th>';
    foreach ($cols as $title) {
      $tooltip = '';
      if ($config['dot_stat_table_column_name']) {
        $tooltip = $title;
        if (strpos($config['dot_stat_table_column_name'], '/') === 0) {
          $title = ucfirst(trim(preg_replace($config['dot_stat_table_column_name'], '', $title)));
        } else if (count($arrcoltitles) > 0) {
          $title = array_shift($arrcoltitles);
        } else {
          $title = $config['dot_stat_table_column_name'];
        }
      }
      $pf .= '<th class="text-align-center"'
        .($tooltip?(' title="'.$tooltip.'" data-toggle="tooltip" data-placement="top"'):'')
        .'>'.ucfirst($title).'</th>';
    }        
    $pf .= '</tr></thead>'
      .'<tbody>';

    $sf = '</tbody></table></div>';
    
    // --- RETURN MARKUP
    $table = $pf.''.implode("\n", $htmlrows).''.$sf;
    $rtrt = $htmlrows;

    return $table;
       
  }





  /**
   * @return string html + js code to run highchart graph
   */
  private function renderChart(){
        
    $meta_field = get_post_meta( $this->dotstat_post->ID);
    $chart_title = isset($meta_field['chart_title']) ? $meta_field['chart_title'][0] : '';
    $chart_subtitle = isset($meta_field['chart_subtitle']) ? $meta_field['chart_subtitle'][0] : '';
    $request_url = isset($meta_field['request_url']) ? $meta_field['request_url'][0] : '';
    $chart_entity = isset($meta_field['chart_entity']) ? intval($meta_field['chart_entity'][0]) : 0;
    $chart_template = isset($meta_field['chart_template']) ? $meta_field['chart_template'][0] : '';
    $charts_configuration = isset($meta_field['charts_configuration']) ? trim(preg_replace("/\r|\n/", "", $meta_field['charts_configuration'][0] )) : '';

    $chart_series_data = get_post_meta( $chart_entity, 'tax_stored_parsed_data', true); 
    $entity_default_link = get_post_meta( $chart_entity, 'default_link');
    $decoded_series_data = json_decode($chart_series_data, true);
    $chart_series = isset($decoded_series_data['series']) ? $decoded_series_data['series'] : "";
    $chart_data = isset($decoded_series_data['data']) ? $decoded_series_data['data'] : "";

    $parser  = new DotstatParser( $chart_series, $chart_data,  $this->depth);
    $handle_parser = $parser->handleData( $chart_template );

    $subtitle =  $chart_subtitle ? $chart_subtitle : '' ;
    $link =  $request_url ? $request_url : ($entity_default_link[0] ? $entity_default_link[0] : '' );

    $randstring = $this->dotstatRandomString();


    $content = "<div id='main".$randstring."' class='chart_dotstat'>"
      ."<div class='chart_dotstat_wraper'>"
      .  "<div id='".$randstring."' class='chart_dotstat_visual'></div>"
      ."</div></div>";

    // ------------ JAVASCRIPT

    $charts_config = $charts_configuration ? $charts_configuration : '{}';

    switch ($chart_template) {
      case 'Drilldown':
        wp_enqueue_script('dotstat_statchart_drilldown');
        $jsclass = 'DotStatChartDrilldown';
        break;
      case 'PopulationPyramid':
        wp_enqueue_script('dotstat_statchart_population_pyramid');
        $jsclass = 'DotStatChartPopulationPyramid';
        break;
      case 'PopulationPyramidRace':
        wp_enqueue_script('dotstat_statchart_population_pyramid_race');
        $jsclass = 'DotStatChartPopulationPyramidRace';
        break;
      case 'TimeSeries':
        wp_enqueue_script('dotstat_statchart_time_series');
        $jsclass = 'DotStatChartTimeSeries';
        break;
      default:
        $jsclass = 'DotStatChart';
        break;    
    }

    wp_enqueue_script('dotstat_statchart');

    $xid = 'statchart_'.$randstring;

    $script = "$xid = new ".$jsclass."(
      \"$randstring\", "
      .($handle_parser['categories']?json_encode($handle_parser['categories']):'"categories"')
      .", "
      .($handle_parser['settings']??'{}')
      .", "
      .($charts_config??'{}')
      .", \"".$chart_title."\""
      .", \"".$subtitle."\""
      .", \"".($link?str_replace('"','',$link):'')."\""
      .");\n";
    
    $script .= "\n$xid.go(\n"
      .($handle_parser['data']?json_encode($handle_parser['data'], JSON_NUMERIC_CHECK):'')
      .",\n"
      .($handle_parser['seriesdata']?json_encode($handle_parser['seriesdata'], JSON_NUMERIC_CHECK):'false')
      .");";

    
    // wait for every external JS to be loaded (Jquery and Highchart) before rendering
    $script = "var $xid = false;\nwindow.addEventListener('load', function() {\n\t".$script."\n});";

    $content .= "<script>$script</script>";


    return  $content;
   
  }
  

  /**
   * @return string html markup of the keystat
  */
  public function renderKeyStat() { 

    $displayId = $this->dotstat_post->ID;

    $config = [];
    $config['label'] = get_post_meta( $displayId, 'keystat_label', true );
    $config['dot_stat_key_format'] = intval(get_post_meta( $displayId, 'keystat_format', true )); 
    $config['dot_stat_key_icon'] = get_post_meta( $displayId, 'keystat_icon', true );
    $config['dot_stat_key_link'] = get_post_meta( $displayId, 'keystat_link', true );
    $dcode = get_post_meta( $displayId, 'keystat_index', true );      
      
    $datasetId = get_post_meta( $displayId, 'chart_entity', true );
    $node = get_post($datasetId);
    $node_dataObj = json_decode(get_post_meta( $node->ID, 'tax_stored_parsed_data', true ), true);

    $title = $config['label'];        
    $country = null;
    
    $legend_prefix = [];
    $legend_separator = '';    
    $cnt = 0;
    $data = [
      'value' => 0,
      'first' => 9999,
      'year' => 0
    ];

    if ( isset($node_dataObj['data'][$dcode]) ) {
      // get key data
      $dataset = $node_dataObj['data'][$dcode];

      if( is_array(  $dataset ) ){
        if (isset($dataset['value'])) {
          // single value
          $data = $dataset;
        } else {
          // multiple values
          foreach ($dataset as $key => $val) {
            $cnt++;
            $data['value'] = round( ((intval($data['value'] * 100 ) + intval( $val['value'] * 100)) / 100) , 2);
            $data['first'] = min(intval($data['first']), intval( $val['year'] ));
            $data['year'] = max(intval($data['year']), intval( $val['year'] ));
          }
        }
      }

    } else {

      // get it all
      $all = $node_dataObj['data'];
          
      foreach ($all as $key => $val) {
        if( is_array( $val) ){
          
          $val = array_values($val); 
          foreach ($val as $k => $a) {
            $cnt++;
            $data['value'] = intval($data['value']) + intval( $a['value'] );
            $data['first'] = min(intval($data['first']), intval($a['year']));
            $data['year'] = max(intval($data['year']), intval($a['year']));
            // error_log($k.': '.$a['value'].' ('.$a['year'].')');
          }
        } else {

          $cnt++;
          $data['value'] = intval($data['value']) + intval( $val['value'] );
          $data['first'] = min(intval($data['first']), intval( $val['year'] ));
          $data['year'] = max(intval($data['year']), intval( $val['year'] ));

        }      

      }
    }
    
    $value = $data['value'];
   
    if (!empty($config['dot_stat_key_function'])) {
      switch ($config['dot_stat_key_function']) {
        case '1':
          // average
          $value = round($value / $cnt);
          $legend_prefix[] = '('.t('Average').')';
          break;
      }
    }

    $icf = '';
    if (!empty($config['dot_stat_key_icon'])) {
      $icf = '<div class="stat-group--icon">'
      .'<span class="dashicons dashicons-'.$config['dot_stat_key_icon'].'"></span>'
       .'</div>';
    }

    if (!empty($value) && !empty($data['year'])) {

      if (is_float($value)) {
        $decimals = ( (int) $value != $value ) ? (strlen($value) - strpos($value, '.')) - 1 : 0;
        $value = number_format($value, $decimals);
      } else if (is_integer($value)) {
        $value = number_format($value, 0);
      }

      switch ($config['dot_stat_key_format']) {
        case 1: // number
          // already dealt with
          break;
        case 2: // rate
          $value = (($value > 0)?'+':'').$value.'%';
          $legend_prefix[] = 'Rate';
          break;
        case 3: // currency ($)
          $value = '$'.$value;
          $legend_prefix[] = 'USD';
          break;
      }
    } else {
      $value = 'n/c';
    }
    
    $pf = '<div class="stat-group--tile">';
    $sf = '</div>';

    // default url from 
    $url = get_post_meta( $node->ID, 'default_link', true );

    if (!empty($config['dot_stat_key_link'])) {
      if ($config['dot_stat_key_link'] == '#') {
        $url = '';
      } else {
        $url = $config['dot_stat_key_link'];
      }
    }
    
    if (!empty($url) && $value != 'n/c') {
      $pf = '<a href="'.$url.'"';
      if (strpos($url, 'http') === 0) {
        $pf .= ' target="_blank"';
      }
      $pf .= ' class="stat-group--tile">';
      $sf = '</a>';
    }

    if (count($legend_prefix) > 0) {
      $legend_separator = ', ';
    }

    if (!empty($data['first']) && $data['first'] != $data['year']) {
      $legend_separator .= $data['first'].'-';
    }

    $markup_value = '<div class="stat-group--nc">n/c</div>';
    if ($value != 'n/c') {
      $markup_value = '<div class="stat-group--value">'.$value.'</div>'
        .'<div class="stat-group--date">'.implode(' ',$legend_prefix).$legend_separator.$data['year'].'</div>';
    }
    
    // return markup
    return '<div class="dotstat_key-container"><div class="stat-group--container">'
    .$icf
    .'<div class="stat-group--body">'
      .'<div class="stat-group--label">'.$title.'</div>'
      .$markup_value
    .'</div>'
  .'</div></div>';
    
       
  }

  /**
   * @return random string
   */
  private function dotstatRandomString(){

      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randstring = '';
      for ($i = 0; $i < 10; $i++) {
          $randstring .= $characters[ rand(0, strlen($characters)-1) ];
      }
      return $randstring;
  }

}