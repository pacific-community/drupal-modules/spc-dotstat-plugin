<?php

class DotstatParser {

  protected $series;
  protected $data;
  protected $depth;
  protected $option;

  public function __construct($series, $data, $depth = 0 ) {
    $this->series = $series;
    $this->data = $data;               
    $this->depth = $depth;
    $this->options = get_option( 'stat_chart_option_data' );
  }
    
  public function handleData($type) {

    $response = [
      'settings'    => '',    // json string
      'data'        => [],    // PHP array
      'seriesdata'  => false, // PHP array or false
      'categories'  => []     // PHP array
    ];

    if (!empty($type) && in_array($type, $GLOBALS['dotstat_chart_templates'])) {

      $response['settings']   = call_user_func(array($this, 'get'.$type.'Settings'));
      $response['data']       = call_user_func(array($this, 'get'.$type.'Data'));
      $response['categories'] = call_user_func(array($this, 'get'.$type.'Categories'));

      if ($type == 'Drilldown') {
        $response['seriesdata'] = $this->getDrilldownSeries();
      }

    }

    return $response;

  }

  // ---------------------- COLUMN CHARTS

  private function getColumnsSettings(){

    if(isset($this->options['column_chart']) && trim( $this->options['column_chart'] )){
      return  trim(preg_replace("/\r|\n/", "", $this->options['column_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/column.json';
      if (file_exists($path)) {
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for column chart" }';
      }
    }
  }

  private function getColumnsCategories() {
    return $this->_commonCategories();
  }

  private function getColumnsData(){
      
    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      foreach ($this->data as $key => $arv) {
        $rawdata[] = $this->_json2data($key, $arv);
      }
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        $data['name'] = $sid;
        $data['label'] = $sdef['name'];
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
      break;
    }

    return $rawdata;
  }

  

  // ---------------------- DRILLDOWN CHARTS

  /**
   * @return string settings for drilldown chart type
   */
  private function getDrilldownSettings(){

    if(isset($this->options['drilldown_chart'])  && trim( $this->options['drilldown_chart'] )){
      return  trim(preg_replace("/\r|\n/", "", $this->options['drilldown_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/drilldown.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for drilldown chart" }';
      }
    }
  }

  /**
   * get categories
   */
  private function getDrilldownCategories() {
    return $this->_commonCategories();
  }

  /**
   * @return array parsed drill data
   */
  private function getDrilldownData(){

    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      // no drilldown when no series
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        $data['name'] = $sid;
        $data['drilldown'] = $sid;
        $data['label'] = $sdef['name'];
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
    }

    return $rawdata;

    

  }

  /**
   * @return array simple parsed drill data
   */
  private function getDrilldownSeries(){

    $series = [];
    $level1 = reset($this->series);
    foreach ($level1['values'] as $obj) {
      $series[ $obj['id']] = $obj['name'];
    }

    $drills = [];

    foreach ($this->data as $drill => $dset) {
      $data = [];
      foreach ($dset as $yid => $drdata) {
        if (!empty($data) && !empty($drdata['year']) && $yid != $drdata['year']) {
          // skip non reference years
          continue;
        }
        $data[] = [ "$yid", $drdata['value'] ];
      }
      $drills[] = (object) [
        'id' =>  $drill,
        'name' => $series[$drill],
        'type' => 'line', // @todo drilldown chart type should be selectable (actually fully configurable)
        'getExtremesFromAll' => true,
        'tooltip' => [
          'headerFormat' => '<em>{series.name}<br />',
          'pointFormat' => '{point.y} in {point.x}'
        ],
        'data' => $data
      ];
    }

    return $drills;

  }
  

  // ---------------------- LOLLIPOP CHARTS

  /**
   * @return string lollipop settings chart
   */
  private function getLollipopSettings(){

    if(isset($this->options['lollipop_chart']) && trim( $this->options['lollipop_chart'] ) ){
      return  trim(preg_replace("/\r|\n/", "", $this->options['lollipop_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/lollipop.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for lollipop chart" }';
      }
    }
  }

  /**
   * get categories
   */
  private function getLollipopCategories() {
    return $this->_commonCategories();
  }

  /**
  * @return array Handle Lolipop chart graf Data
  */
  private function getLollipopData(){

    $rawdata = [];
    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      foreach ($this->data as $key => $arv) {
        $rawdata[] = $this->_json2data($key, $arv);
      }
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $data = [];
        $sid = $sdef['id'];
        foreach ($this->data[ $sid ] as $key => $arv) {
          // overwrite to keep only last value
          $data = $this->_json2data($key, $arv);
        }
        if (empty($data['y'])) {
          $data['y'] = '0';
        }
        $data['name'] = $sid;
        $data['label'] = $sdef['name'];
        $rawdata[$sid] = $data;
      }
      ksort($rawdata);
      $rawdata = array_values($rawdata);
    }

    return $rawdata;
  }


  // ---------------------- PIE CHARTS

  /**
   * @return string Pie chart settings
   */
  private function getPieSettings() {

    if (isset($this->options['pie_chart'])  && trim( $this->options['pie_chart'] )){
      return  trim(preg_replace("/\r|\n/", "", $this->options['pie_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/pie.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for pie chart" }';
      }
    }
  }

  /**
   * get categories
   */
  private function getPieCategories() {
    return $this->_commonCategories();
  }

  /**
   * @return array parsed Pie chart DATA
   */
  private function getPieData() {
    return $this->getColumnsData();
  }

  // ---------------------- PIE SUMMARY CHARTS

  /**
   * @return string PieSummary chart settings
   */
  private function getPieSummarySettings() {

    if(isset($this->options['piesummary_chart']) && trim( $this->options['piesummary_chart'] )){

      return  trim(preg_replace("/\r|\n/", "", $this->options['piesummary_chart']));

    } else {
      $path = plugin_dir_path( __DIR__ ).'config/pie_summary.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for pie summary chart" }';
      }
    }
  }

  /**
   * get categories
   */
  private function getPieSummaryCategories() {
    return $this->_commonCategories();
  }

  /**
   * @return array parsed PieSummary chart DATA
  */
  private function getPieSummaryData(){
    $data = [
      'Yes' => [
        'name' => 'Yes',
        'label' => '',
        'color' => '#6c6',
        'y' => 0
      ],
      'No' => [
        'name' => 'No',
        'label' => '',
        'color' => '#f66',
        'y' => 0
      ],
      'No answer' => [
        'name' => 'No answer',
        'label' => '',
        'color' => '#aaa',
        'y' => 0
      ]
    ];

    $countries = [
      // 'AS' => 'American Samoa', 
      'CK' => 'Cook Islands',
      'FJ' => 'Fiji',
      'FM' => 'Micronesia (Federated States of)',
      // 'GU' => 'Guam',
      'KI' => 'Kiribati',
      'MH' => 'Marshall Islands',
      // 'MP' => 'Northern Mariana Islands',
      'NC' => 'New Caledonia',
      'NR' => 'Nauru',
      'NU' => 'Niue',
      'PF' => 'French Polynesia',
      'PG' => 'Papua New Guinea',
      // 'PN' => 'Pitcairn Islands',
      'PW' => 'Palau',
      'SB' => 'Solomon Islands',
      // 'TK' => 'Tokelau',
      'TO' => 'Tonga',
      'TV' => 'Tuvalu',
      'VU' => 'Vanuatu',
      // 'WF' => 'Wallis and Futuna',
      'WS' => 'Samoa'
    ];

    switch ($this->depth) {
    case 0:
      // --------------- NO SERIES
      // @todo
      break;
    case 1:
      // --------------- 1-LEVEL SERIES
      $s1 = reset($this->series);
      foreach($s1['values'] as $sdef) {
        $sid = $sdef['id'];
        // take last one only
        $last = array_pop($this->data[$sid]);
        if (array_key_exists($sid, $countries)) {
          switch ($last['value']) {
            case '0':
              $data['No']['label'] .= '<br />- '.$countries[$sid];
              $data['No']['y'] += 1;
              break;
            case '1':
            case '100':
              $data['Yes']['label'] .= '<br />- '.$countries[$sid];
              $data['Yes']['y'] += 1;
              break;
          }
        }
        unset($countries[$sid]);
      }

      // count missing ones
      foreach ($countries as $cid => $name) {
        $data['No answer']['label'] .= '<br />- '.$name;
        $data['No answer']['y'] += 1;
      }

      // return values
      $data = array_values($data);
    }

    return $data;

  }


  // ---------------------- POPULATION PYRAMID

  /**
   * @return string  PopulationPyramidSettings
   */
  private function getPopulationPyramidSettings() {

    if (isset($this->options['populationpyramid_chart']) && trim( $this->options['populationpyramid_chart'] )) {
      return  trim(preg_replace("/\r|\n/", "", $this->options['populationpyramid_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/population_pyramid.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for population pyramid chart" }';
      }
    }
    
  }

  /**
   * @return array Population categories
  */
  private function getPopulationPyramidCategories(){
   
    $cats = [];

    // in this case, categories are age ranges 
    // coming from dataset, not from series definition

    $dcopy = $this->data;

    // assuming all datasets use the same dimensions (same are ranges)
    $dg = array_shift($dcopy); // Gender (F or M) dataset

    foreach ($dg as $k => $arv) {
      $cats[] = $arv['age'];
    }
    
    return $cats;
  }

  /**
   * @return array PopulationPyramid decoded Data
   */
  private function getPopulationPyramidData(){
    	// rawdata
      $fulldata = [];
      foreach ($this->data as $kg => $arr) {
        $data = [];
        foreach ($arr as $k => $arv) {
          $data[] = $arv['value'];
        }
        $fulldata[ $kg ] = $data;
      }
      return $fulldata;
  }


  // ---------------------- POPULATION PYRAMID MULTI

  /**
   * @return string PopulationPyramidMultiSettings settings 
   */
  private function getPopulationPyramidMultiSettings() {
    if (isset($this->options['populationpyramidmulti_chart']) && trim( $this->options['populationpyramidmulti_chart'] )) {
      return  trim(preg_replace("/\r|\n/", "", $this->options['populationpyramidmulti_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/population_pyramid_multi.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for population pyramid multi chart" }';
      }
    }
  }

  /**
   * @return array PopulationPyramidMultiCateg category 
   */
  private function getPopulationPyramidMultiCategories() {

    $cats = [];

    // in this case, categories are age ranges 
    // coming from dataset, not from series definition

    $dcopy = $this->data;

    // assuming all series use the same dimensions (same age ranges)
    $d1 = array_shift($dcopy); // first year on dataset
    $d2 = array_shift($d1); // Gender (F or M) dataset

    foreach ($d2 as $k => $arv) {
      $cats[] = $arv['age'];
    }
    
    return $cats;
  }

  /**
   * @return array  PopulationPyramidMulti parsed data
   */
  private function getPopulationPyramidMultiData(){

    // expecting SERIES depth 2:
    // 1) TIME PERIOD (year)
    // 2) GENDER (F/M)
    $fulldata = [];
    foreach ($this->data as $ky => $arr) {
      $series = array_keys($arr);
      $datayear = [];
      $step = 0;
      foreach($series as $ks) {
        $data = [];
       
        foreach ($arr[$ks] as $k => $arv) {
          // $data[] = $arv['value'];
          if ( $step % 2) {
            $data[] = -1 * $arv['value'] ;
          } else {
            $data[] = $arv['value'];
          }

        }
        $datayear[] = array('name' => $ks, 'data'=> $data);
        $step++;
      }
      $fulldata['steps'][] = $ky;
      $fulldata[$ky] = $datayear;
    }

    return $fulldata;

  }
  

  // ---------------------- POPULATION PYRAMID RACE

  public function getPopulationPyramidRaceSettings() {

    if (isset($this->options['population_pyramid_race_chart']) && trim( $this->options['population_pyramid_race_chart'] )) {
      return  trim(preg_replace("/\r|\n/", "", $this->options['population_pyramid_race_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/population_pyramid_race.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for population pyramid race chart" }';
      }
    }
  }

  public function getPopulationPyramidRaceCategories() {

    $cats = [];

    // in this case, categories are age ranges 
    // coming from dataset, not from series definition

    $dcopy = $this->data;

    // assuming all series use the same dimensions (same age ranges)
    $d1 = array_shift($dcopy); // first year on dataset
    $d2 = array_shift($d1); // Gender (F or M) dataset

    foreach ($d2 as $k => $arv) {
      $cats[] = $arv['age'];
    }
    
    return $cats;
  }

  public function getPopulationPyramidRaceData() {
    // expecting SERIES depth 2:
    // 1) TIME PERIOD (year)
    // 2) GENDER (F/M)
    $fulldata = [];
    foreach ($this->data as $ky => $arr) {
      $series = array_keys($arr);
      $datayear = [];
      foreach($series as $ks) {
        $data = [];
        foreach ($arr[$ks] as $k => $arv) {
          $data[] = $arv['value'];
        }
        $datayear[ $ks ] = $data;
      }
      $fulldata[ $ky ] = $datayear;
    }
    return $fulldata;
  }

  // ---------------------- TIME SERIES CHART

  /**
   * @return string  TimeSeries chart settings
   */
  private function getTimeSeriesSettings() {
    if(isset($this->options['timeseries_chart']) && trim( $this->options['timeseries_chart'] )){
      return  trim(preg_replace("/\r|\n/", "", $this->options['timeseries_chart']));
    } else {
      $path = plugin_dir_path( __DIR__ ).'config/time_series.json';
      if (file_exists($path)) {     
        $jstring = file_get_contents($path); // non PHP compatible string
        return preg_replace("/\r|\n/", "", $jstring); 
      } else {
        return '{ error: "no config for time series chart" }';
      }
    }
  }

  /**
   * @return array  TimeSeries Categories
   */
  private function getTimeSeriesCategories() {

    $arrcat = [];
    foreach($this->data as $ccode => $cdata) {
      foreach ($cdata as $key => $arv) {
        if (!in_array($key, $arrcat)) {
          $arrcat[] = $key;
        }
      }
    }
    sort($arrcat);
    return $arrcat;

  }

  /**
   * @return array  TimeSeries parsed data
   */
  private function getTimeSeriesData() {

    $rawdata = [];
  
    $s1 = reset($this->series);
    foreach($s1['values'] as $sdef) {
      $data = [];
      $sid = $sdef['id'];        
      foreach ($this->data[ $sid ] as $key => $arv) {
        $data['id'] = $sdef['id'];
        $data['name'] = $sdef['name'];
        $data['data'][] = $this->_json2datatimeseries($sdef['name'], $arv, ['year' => 'x', 'date' => 'x']);
      }
      $rawdata[] = $data;
      ksort($rawdata);
    }
    return $rawdata;

  }



  // ---------------------- HELPERS METHODS

  /**
   * Parse JSON data to render data for Highcharts
   *
   * @param $name name of data serie
   * @param $obj JSON object holding data
   * @param $map map object properties to rendered properties
   *
   * @return array representing a single row of data
   */
  protected function _json2data($name, $obj, $map='') {
    // check file map
    if (!$map) {
      $map = [];
    }
    // get name from index
    $data = [
      'name' => $name
    ];
    // get value
    $val = 'n/c';
    foreach ($obj as $k => $v) {
      $kt = $k;
      if (array_key_exists($k, $map)) {
        $kt = $map[$k];
      } else {
        switch($k) {
          case 'value':
            // get value
            $kt = 'y';
            // save for formatted value
            $val = $v;
            break;
        }
      }
      $data[$kt] = $v;
    }
    if (is_numeric($val)) {
      // format value (y)
      $data['formatted'] = number_format($val);
    }
    return $data;
  }
  
  /**
   * 
   * Function For time series chart
   */
  protected function _json2datatimeseries($name, $obj, $map='') {
    // check file map
    if (!$map) {
      $map = [];
    }
    // get name from index
    $data = [
      'label' => $name
    ];
       
    // get value
    $val = 'n/c';
    foreach ($obj as $k => $v) {
      $kt = $k;
      if (array_key_exists($k, $map)) {
        $kt = $map[$k];
      } else {
        switch($k) {
          case 'value':
            // get value
            $kt = 'y';
            // save for formatted value
            $val = $v;
            break;
        }
      }
      if($kt == "x" and $k == "year"){
        # if year provided, use it as int value
        $data[$kt] = DateTime::createFromFormat("Y", $v);
      } else if($kt == "x" and $k == "date"){
        # if date is provided, try to convert it to Datetime object
        if(count(explode("-", $v)) == 3){
          $data[$kt] = DateTime::createFromFormat("Y-m-d", $v);
        } else if(count(explode("-", $v)) == 2){
          $data[$kt] = DateTime::createFromFormat("Y-m", $v);
        } else {
          $data[$kt] = DateTime::createFromFormat("Y", $v);
        }
      } else {
        $data[$kt] = $v;
      }
      
    }
    if (is_numeric($val)) {
      // format value (y)
      $data['formatted'] = number_format($val);
    }
    return $data;
  }

  /**
	 * Get Highchart formatted categories from either data or series 
	 * 
	 * @return mixed array of categories or string setting mode
	 */
	private function _commonCategories() {
		switch ($this->depth) {
    case 0:
    	// --------------- NO SERIES
    	// get key from data index
      $categories = array_keys($this->data);
      break;
    default:
    	// --------------- DATA SERIES
      // get keys from dimensions definitions
    	// use dimension ID by default (not name)
    	$level1 = reset($this->series);
    	foreach ($level1['values'] as $obj) {
    		$categories[] = $obj['id'];
    	}
    	break;
    }
    return $categories;
  }
  

}

