<?php


/**
 * @return array of the objects with .STAT Display posts
 */
function dotstat_taxcpt_get_display(){
  $query = new WP_Query;
  $dotstat_posts = $query->query( array(
    'post_type' => 'dotstat',
    'posts_per_page' => -1
  ) );
  $postsDisplay = array(); 

  foreach( $dotstat_posts as $pst ){
    $post = new class{};
    $post->id = $pst->ID;
    $post->title = $pst->post_title;
    $postsDisplay[] = $post;
  
  }
  wp_send_json( array('message' => 'All posts', 'posts' => $postsDisplay, 'code' => 200));
}

add_action('wp_ajax_taxcpt_get_display', 'dotstat_taxcpt_get_display');


/**
 * Run Cron Event
 * Updates all datasets post
 */
function dotstat_taxcpt_request_and_update() {

  $query = new WP_Query;
  $tax_dotstat_posts = $query->query( array(
    'post_type' => 'tax_dotstat',
    'posts_per_page' => -1
  ) ); 

  foreach( $tax_dotstat_posts as $pst ){
    $request_url = get_post_meta( $pst->ID, 'request_url', true );
    $taxparser_config = get_post_meta( $pst->ID, 'taxparser_config', true );
    
    dotstat_update_request($pst->ID, $request_url, $taxparser_config);      
  }
    
}

add_action('dotstat_daily_request_and_update', 'dotstat_taxcpt_request_and_update');


/**
 * Query .stat and update parse data for a single dataset post
 */
function dotstat_update_request($postId, $url, $param){  
        
  $param = (empty($param))?'':preg_replace("/\r|\n/", "", $param);
      
  $request_url = $url;
  $post_id = intval( $postId );

  $post = get_post( $post_id);

  if( ! $post || $post->post_type != 'tax_dotstat'  )   return;                

  if (filter_var($request_url, FILTER_VALIDATE_URL)) {
      
    $request = new StatSDSRequest();

    $resp = $request->sendRequest($request_url, $param);

    if( isset($resp['parsed']) && $resp['parsed'] ){

      update_post_meta( $post->ID, 'tax_stored_parsed_data', $resp['parsed'] ); 
    }              

  }     
   
}



/**
 * Run request and store parsed data to post
 * 
 */
function dotstat_taxcpt_store_refresh(){
  
  if( ! wp_verify_nonce( $_POST['nonce_stat'], 'chart_refresh_nonce') || ! $_POST['request_url'] || !$_POST['post_id'] ){
    wp_send_json( array('message'   => 'Wrong nonce code or missed required parameters', 'code' => 401));
  }
      
  $request_url = $_POST['request_url']??'';
  $param = (empty($_POST['param']))?'':preg_replace("/\r|\n/", "", $_POST['param']);
  $post_id = intval( $_POST['post_id'] );

  $post = get_post( $post_id);

  if( ! $post || $post->post_type != 'tax_dotstat'  ){
      wp_send_json( array('message'   => 'Post not found or incorrect post type', 'data' => '', 'code' => 404));
  }

  if (filter_var($request_url, FILTER_VALIDATE_URL)) {
      
    $request = new StatSDSRequest();

    $resp = $request->sendRequest($request_url, $param);

    if ( !empty($resp['parsed']) ) {

      update_post_meta( $post->ID, 'request_url', $request_url );
      update_post_meta( $post->ID, 'taxparser_config', $_POST['param']??'' );
      update_post_meta( $post->ID, 'tax_stored_parsed_data', $resp['parsed'] );
      update_post_meta( $post->ID, 'default_link', $_POST['default_link']??'');
      
      wp_send_json( array('message' => "Data stored to post ({$post->post_status})", 'data' => $resp['parsed'],  'code' => 200) );

    } else {
      wp_send_json( array('message' => $resp['msg']??'Error parsing data', 'data' => '', 'code' => 404) );
    }                    
    

  } else {
      wp_send_json( array('message' => 'Not valid URL', 'data' => '', 'code' => 404));
  } 

  die();

}

add_action('wp_ajax_taxcpt_store_refresh', 'dotstat_taxcpt_store_refresh');



/**
 * Check PDH.stat query
 * returns partially parsed response for data structure and dataset analysis
 */
function dotstat_taxcpt_request_url() {

  if( ! wp_verify_nonce( $_POST['nonce_stat'], 'chart_refresh_nonce') || ! $_POST['request_url'] ){
    wp_send_json( array('message'   => 'Wrong nonce code or missed required parameters', 'code' => 401));
  }

  // checking param
  // if empty, simply checking API request
  // if set, parsing JSON while we're at it
  $param = (empty($_POST['param']))?'':preg_replace("/\r|\n/", "", $_POST['param']);
    
  $request_url = $_POST['request_url'];

  if (filter_var($request_url, FILTER_VALIDATE_URL)) {
      
    $request = new StatSDSRequest();

    $resp = $request->sendRequest($request_url, $param);
                   
    wp_send_json( array(
      'message' => $resp['msg'],
      'resp' => $resp['responce'],
      'structure_table' => $resp['structure_table'],
      'attributes' => $resp['attributes'],
      'parsed' => $resp['parsed'],
      'code' => $resp['code']) 
    );

  } else {
    wp_send_json( array(
      'message' => 'Not valid URL',
      'resp' => '', 'parsed' => '', 
      'structure_table' => '',
      'attributes' => '',
      'code' => 404)
    );
  } 

  die();
}

add_action('wp_ajax_taxcpt_request_url', 'dotstat_taxcpt_request_url');


/**
 * Check dataset's entries for keystat displays
 * 
 */
function dotstat_keystat_datasets(){
  
  if( ! wp_verify_nonce( $_POST['nonce_stat'], 'chart_refresh_nonce') || ! $_POST['data_id']){
    wp_send_json( array('message'   => 'Wrong nonce code or missed required parameters', 'code' => 401));
    die();
  }

  $data = dotstat_list_datasets( intval( $_POST['data_id'] ) );

  if (is_array($data)) {
    // data found
    wp_send_json( array(
      'message' => count($data)." data found",
      'data' => $data,
      'code' => 200 
    ) );
  
  } else {
    $arm = [
      'data' => '',
      'code' => $data
    ];
    switch($data) {
      case '404':
        $arm[ message] = 'Post not found or incorrect post type';
        break;
      case '502':
        $arm[ message] ='Error parsing data';
        break;
    }
    wp_send_json( $arm );
  }

  die();

}

add_action('wp_ajax_dotstat_keystat_datasets', 'dotstat_keystat_datasets');



/****************************************************************************
 *                                                                          *
 * StatSDSRequest Class                                                     *
 *                                                                          *
 * Sends requests to PDH.stat and parses response                           *
 *                                                                          *
 ****************************************************************************/


class StatSDSRequest { 

 /**
  *  function send remote get request to URL
  */
  public function sendRequest($url, $param = '') {

    $prepare_url = str_replace("&amp;", "&", $url);
    $clearurl = str_replace("amp;", "", $prepare_url);
    $clearparam = stripslashes($param);
    
    $response = array(
      'code'            => 500,
      'msg'             => 'Unknow Error',
      'responce'        => '',
      'structure_table' => '',
      'attributes'      => '',
      'parsed'          => ''
    );

    $clearurl .= (parse_url($clearurl, PHP_URL_QUERY) ? '&' : '?') . 'format=jsondata';


    $remote_get = wp_remote_get( $clearurl, array(
      'sslverify'   => false
    ));

    if (is_wp_error($remote_get)) {
      // request didn't get through
      $response['code'] = '502';
      $response['msg']  = implode(', ', $remote_get->get_error_messages());
      return $response;
    }

    $resp_code = wp_remote_retrieve_response_code(  $remote_get );
    $headers =  wp_remote_retrieve_headers(  $remote_get );
    $body = wp_remote_retrieve_body( $remote_get );

    $type = $headers['Content-Type'] ?? '';

    if ($resp_code == 200 && preg_match("/application\/.*json/", $type)) {
                
      $parsed = '';
      if (!empty($clearparam)) {
        // parsing request
        $parsed = $this->getParsedResults($body, $clearparam);
      }

      $encoded_body = json_decode($body);

      $structure_table =  $this->structureIdName( $encoded_body );
      $attributes =  $this->parseAttribute($encoded_body);

      $response['code']             = 200;
      $response['msg']              = 'Response Output';
      $response['responce']         = $encoded_body;
      $response['structure_table']  = $structure_table;
      $response['attributes']       = $attributes;
      $response['parsed']           = $parsed;
      
      return $response;
      
    } else {
      $response['code']   = $resp_code;
      $response['msg']    = 'Response is not JSON object';
      return $response;
    }  
  }


  /**
   *  Parse result do JSON
   */
  private function getParsedResults($res, $params = '') {
  
    $raw = json_decode($res, true);
    $conf = json_decode($params, true);
    $data = [];

    if (array_key_exists('structure', $raw['data'])) {
      $structure = $raw['data']['structure'];
    } else if(array_key_exists('structures', $raw['data'])) {
      $structure = $raw['data']['structures'][$raw['data']['dataSets'][0]['structure']];
    }

    $dimensions = $structure['dimensions']['observation'];
    $attributes = $structure['attributes']['observation'];

    if (empty($conf['data'])) {
      // old style config backward compatibility
      $conf['data'] = $conf;
    }

    if (empty($conf['series'])) {
      // old style config backward compatibility
      $conf['series'] = [];
    }

    if (empty($conf['filter'])) {
      // old style config backward compatibility
      $conf['filter'] = [];
    }
    

    if (count($conf['series']) == 0) {

      // --------------- SINGLE DATA SET

      foreach ($raw['data']['dataSets'][0]['observations'] as $key => $arv) {
        $res = $this->parseRow($key, $arv, $conf, $dimensions, $attributes);
        if (!$res) {
          // either empty or filtered
          continue;
        }
        if ($conf['data']['index']) {
          $idx = $res['index'];
          if (!empty($data[$idx]) && empty($res['value'])) {
            continue;
          }
          unset($res['index']);
          $data[$idx] = $res;
        } else {
          $data[] = $res;
        }
      }

      if ($conf['data']['index']) {
        ksort($data);
      }

      return json_encode(array('data' => $data), JSON_PRETTY_PRINT);

    } else {
      
      // --------------- MULTIPLE DATA SERIES

      // ----- PARSE SERIES CONFIG

      $confseries = [];

      if (is_string($conf['series'])) {
        // OLD METHOD (STRING) e.g. "dim[5].id, dim[2].id"

        $arrseries = explode(',', $conf['series']);
        foreach ($arrseries as $str) {
          $str = trim($str);
          $confseries[] = $str;
        }
      } else {
        // NEW METHOD (ARRAY) e.g. { "dim[2]", "dim[5]" }
        foreach ($conf['series'] as $str) {
          $confseries[] = $str;
        }
      }

      // ----- COMPILE SERIES STRUCTURE

      $dataseries = []; // dimensions (series) definitions

      foreach ($confseries as $sdef) {

        // parse index
        $tst = preg_match('/^(dim)\[(\d+)\](\.(.+))?$/', $sdef, $ari);
        if (!$tst) {
          // no index defined
          //\Drupal::logger('spc_dot_stat_data')->error('SERIES MISCONFIGURATION: '.$sdef);
          continue;
        }

        // Series Dimensions
        $sidx = $ari[2]; // dim[X]
        $sidi = (isset($ari[4]))?$ari[4]:'id'; // dim[1].XXXX
        $sidn = 'name';

        // dimensions values
        $dims = $dimensions[$sidx]['values'];

        $dst = [];
        foreach ($dims as $dimidx => $dimobj) {
          $dst[ $dimidx ] = [ 
            'id'    => $dimobj[ $sidi ],
            'name'  => $dimobj[ $sidn ]
          ];
        }

        // dimensions definitions
        $dataseries[ $sidx ] = [
          'id'     => $dimensions[ $sidx ]['id'],
          'name'   => $dimensions[ $sidx ]['name'],
          'values' => $dst
        ];

      }

      // ----- COMPILE DATA (OBSERVATIONS)

      $test_keys = array_keys($dataseries);

      foreach ($raw['data']['dataSets'][0]['observations'] as $key => $arv) {

        $tst = explode(':',$key);
        $res = $this->parseRow($key, $arv, $conf, $dimensions, $attributes);

        if (!$res) {
          // either empty or filtered
          continue;
        }

        switch(count($dataseries)) {
          case 1:
            $n = $tst[ $test_keys[0] ];
            $nlb = $dataseries[ $test_keys[0] ]['values'][$n];
            // error_log("ROW $key test: $n = $nlb");
            if ($conf['data']['index']) {
              $idx = $res['index'];
              unset($res['index']);
              $data[ $nlb['id'] ][ $idx ] = $res;
              // error_log('> added in '.$nlb.'['.$idx.'] as '.$res['value']);
            } else {
              $data[ $nlb['id'] ][] = $res;
            }
            break;
          case 2:
            $n = $tst[ $test_keys[0] ];
            $nlb = $dataseries[ $test_keys[0] ]['values'][$n];
            $m = $tst[ $test_keys[1] ];
            $mlb = $dataseries[ $test_keys[1] ]['values'][$m];
            // $data[$n][$m][] = $res;
            if ($conf['data']['index']) {
              $idx = $res['index'];
              unset($res['index']);
              $data[ $nlb['id'] ][ $mlb['id'] ][ $idx ] = $res;
            } else {
              $data[ $nlb['id'] ][ $mlb['id'] ][] = $res;
            }
            
            break;
        }

      } // end loop rows

      // sort results by key
      if ($conf['data']['index']) {
        switch(count($dataseries)) {
          case 1:
            foreach($data as $k => $d) {
              ksort($d);
              $data[$k] = $d;
            }
            break;
          case 2:
            foreach($data as $k1 => $d1) {
              foreach($d1 as $k2 => $d2) {
                ksort($d2);
                $d1[$k2] = $d2;
              }
              $data[$k1] = $d1;
            }
            ksort($data);
            break;
        }
      }

      // return JSON
      return json_encode(
        array(
          'series' => $dataseries,
          'data' => $data
        ),
        JSON_PRETTY_PRINT
      );
      
    }

    return json_encode($data);

  }

  private function parseKeyValue($key, $param, $rowkey, $values, $dims, $attrs) {
    $result = null;
    if ($param == 'value') {
      $param = 'val[0]'; // alias
    }
    if (preg_match('/^(dim|val|attr)\[(\d+)\](\.(.+))?$/', $param, $red)) {
      switch ($red[1]) {
        case 'dim':
          // gets data from dimensions structure, indexed from observations' key at certain position
          // dim[position].property eg. dim[3].name
          $i = $red[2];
          $n = explode(':',$rowkey)[$i];
          $p = $red[4];
          $result = isset($n, $p)?$dims[$i]['values'][$n][$p]:null;
          break;
        case 'attr':
          // look for attributes
          // attr[position].property eg. attr[4].name
          $i = $red[2];
          $n = $values[$i];
          $p = $red[4];
          $result = isset($n, $p)?$attrs[$i-1]['values'][$n][$p]:null;
          break;
        case 'val':
          // gets data from array of observations's value
          // val[position] eg. val[0]
          $i = $red[2];
          $result = $values[$i];
          break;
      }
    }
    return $result;
  }

  /**
   * Parse data
  */
  private function parseRow($rowkey, $values, $conf, $dims, $attrs) {

    $data = [];

    // filtering
    if (!empty($conf['filter'])) {
      //echo '<pre>'; var_dump($attrs); die('</pre>');
      foreach ($conf['filter'] as $k => $p) {
        // filter format : [!]?value
        $neg = (strpos($p, '!') === 0)?true:false;
        $p = ($neg)?substr($p, 1):$p;
        $val = $this->parseKeyValue($k, $p, $rowkey, $values, $dims, $attrs);
        if (!is_null($val)) {
          if (
            (!$neg && $fv != $fl) 
            || ($neg && $fv==$fl)
          ) {
            // filter do not match
            return false;
          }
        }
      }
    }

    // get value
    foreach ($conf['data'] as $k => $p) {
      $data[$k] = $this->parseKeyValue($k, $p, $rowkey, $values, $dims, $attrs);
    }
    
    return $data;
  }

  /**
   * function create array  from dimensions -> observation Data
   * return array()
   */
  private function structureIdName( $obj ){
     
    $arrayIdName = array();
    if( isset($obj->data->structure)) {
      $structure = $obj->data->structure;
    } else if ( isset($obj->data->structures)) {
      $structure = $obj->data->structures[$obj->data->dataSets["0"]->structure];
    } else {
      return array();
    }

    foreach($structure->dimensions->observation as $key ){
        if(isset($key->id) && isset($key->name) ){
            $arrayIdName[$key->id] = $key->name;
        }

    }
    
    return $arrayIdName;
  }
  
  /**
   * function create array  from dimensions -> observation Data
   * return array()
   */
  private function parseAttribute( $obj ){
     
    $observationArr = array();
    if( isset($obj->data->structure)) {
      $structure = $obj->data->structure;
    } else if ( isset($obj->data->structures)) {
      $structure = $obj->data->structures[$obj->data->dataSets["0"]->structure];
    } else {
      return array();
    }


    foreach( $structure->attributes->observation as $key ){
        if(isset($key->id) && isset($key->name) ){
            $observationArr[$key->id] = $key->name;
        }
    }
    
    return $observationArr;
  }

}

