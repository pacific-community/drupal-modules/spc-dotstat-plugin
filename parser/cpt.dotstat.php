<?php 


if ( ! function_exists('dotstat_post_type') ) {

    // Register Custom Post Type
    function dotstat_post_type() {
        
        $labels = array(
            'name'                  => _x( '.STAT Display', 'Post Type General Name', 'dotstat' ),
            'singular_name'         => _x( '.STAT Display', 'Post Type Singular Name', 'dotstat' ),
            'menu_name'             => __( '.STAT Display', 'dotstat' ),
            'name_admin_bar'        => __( '.STAT Display', 'dotstat' ),
            'archives'              => __( '.STAT Display Archive', 'dotstat' ),
            'attributes'            => __( '.STAT Display Attributes', 'dotstat' ),
            'parent_item_colon'     => __( '', 'dotstat' ),
            'all_items'             => __( 'All .STAT Display', 'dotstat' ),
            'add_new_item'          => __( 'New .STAT Display', 'dotstat' ),
            'add_new'               => __( 'Add new', 'dotstat' ),
            'new_item'              => __( 'New .STAT Display', 'dotstat' ),
            'edit_item'             => __( 'Edit .STAT Display', 'dotstat' ),
            'update_item'           => __( 'Update .STAT Display', 'dotstat' ),
            'view_item'             => __( 'View .STAT Display', 'dotstat' ),
            'view_items'            => __( 'View .STAT Display', 'dotstat' ),
            'search_items'          => __( 'Search .STAT Display', 'dotstat' ),
            'not_found'             => __( 'Not found any .STAT Display', 'dotstat' ),
            'not_found_in_trash'    => __( 'Not found any .STAT Display', 'dotstat' ),
            'featured_image'        => __( 'Featured image of .STAT Display', 'dotstat' ),
            'set_featured_image'    => __( 'Set featured image for .STAT Display', 'dotstat' ),
            'remove_featured_image' => __( 'Remove featured image', 'dotstat' ),
            'use_featured_image'    => __( 'Set featured image', 'dotstat' ),
            'insert_into_item'      => __( 'Add to .STAT Display', 'dotstat' ),
            'uploaded_to_this_item' => __( 'Uploaded to this .STAT Display', 'dotstat' ),
            'items_list'            => __( 'List .STAT Display', 'dotstat' ),
            'items_list_navigation' => __( '', 'dotstat' ),
            'filter_items_list'     => __( '', 'dotstat' ),
        );
        
        $args = array(
            'label'                 => __( 'dotstat', 'dotstat' ),
            'description'           => __( '.STAT Display post type', 'dotstat' ),
            'labels'                => $labels,
            'supports'              => array( 'title'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'show_in_rest'          => true,
            'can_export'            => false,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => false,
            'capability_type'       => 'page',
            'menu_icon'             => 'dashicons-analytics'
        );
        register_post_type( 'dotstat', $args );
        
    }
    add_action( 'init', 'dotstat_post_type', 0 );

}


if(!function_exists('add_dotstat_metaboxes')){

    add_action( 'add_meta_boxes', 'add_dotstat_metaboxes' );

    function add_dotstat_metaboxes(){
        add_meta_box(
            'dotstat_chart_data',
            'Chart fields',
            'dotstat_chart_data',
            'dotstat',
            'normal',
            'default'
        );                          
       
    }
}


/**
 * Output the HTML for the metabox.
 */
function dotstat_chart_data() {
	global $post;

    $iconset = [
        'groups'            => 'People',
        'admin-users'       => 'Person',
        'businessman'       => 'Person (Male)',
        'businesswoman'     => 'Person (Female)',
        'admin-site'        => 'Globe (US)',
        'admin-site-alt'    => 'Globe (EU)',
        'admin-site-alt2'   => 'Globe (AU)',
        'admin-site-alt3'   => 'Globe (Net)',
        'dashboard'         => 'Dashboard',
        'admin-post'        => 'Pin',
        'admin-appearance'  => 'Brush',
        'admin-tools'       => 'Wrench',
        'admin-home'        => 'House',
        'admin-multisite'   => 'Houses (x3)',
        'admin-network'     => 'Key',
        'admin-generic'     => 'Clog',
        'database'          => 'Database',
        'database-export'   => 'Database (export)',
        'database-import'   => 'Database (import)',
        'welcome-learn-more'=> 'Education',
        'visibility'        => 'Eye',
        'trash'             => 'Trash',
        'flag'              => 'Flag',
        'chart-pie'         => 'Chart (Pie)',
        'chart-bar'         => 'Chart (Columns)',
        'chart-area'        => 'Chart (Area)',
        'lightbulb'         => 'Light bulb',
        'phone'             => 'Phone',
        'cart'              => 'Shopping cart',
        'airplane'          => 'Plane',
        'money-alt'         => 'Money',
        'carrot'            => 'Carrot',
        'car'               => 'Car',
        'coffee'            => 'Coffee',
        'food'              => 'Resaurant',
        'pets'              => 'Animal (print)',
        'bell'              => 'Bell',
        'awards'            => 'Certificate',
        'palmtree'          => 'Palm tree',
        'tickets-alt'       => 'Ticket',
        'location-alt'      => 'Map location',
        'sort'              => 'Arrows (up and down)',
        'leftright'         => 'Arrows (left and right)',
        'randomize'         => 'Arrows (crossing)',
        'update-alt'        => 'Arrows (circular)'
    ];

	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'dotstat_fields' );

    // Get the location data if it's already been entered
    $dotstat_type_display = get_post_meta( $post->ID, 'type_display', true );
    $chart_title = get_post_meta( $post->ID, 'chart_title', true );
    $request_url = get_post_meta( $post->ID, 'request_url', true );
    $chart_subtitle = get_post_meta( $post->ID, 'chart_subtitle', true );
    $chart_entity = get_post_meta( $post->ID, 'chart_entity', true );
    $chart_template = get_post_meta( $post->ID, 'chart_template', true );
    $charts_configuration = get_post_meta( $post->ID, 'charts_configuration', true );

    // $data_depth = get_post_meta( $post->ID, 'data_depth', true );

    // Table meta
    $dot_stat_table_title = get_post_meta( $post->ID, 'dot_stat_table_title', true );
    $dot_stat_table_column_name = get_post_meta( $post->ID, 'dot_stat_table_column_name', true );
    $dot_stat_table_header_css = get_post_meta( $post->ID, 'dot_stat_table_header_css', true );
    $dot_stat_table_link = get_post_meta( $post->ID, 'dot_stat_table_link', true );
    // $table_label = get_post_meta( $post->ID, 'table_label', true );
    $dot_stat_table_format = get_post_meta( $post->ID, 'dot_stat_table_format', true );
    // KeyStat Meta
    $keystat_label = get_post_meta( $post->ID, 'keystat_label', true );
    $keystat_format = get_post_meta( $post->ID, 'keystat_format', true );
    $keystat_icon = get_post_meta( $post->ID, 'keystat_icon', true );
    $keystat_link = get_post_meta( $post->ID, 'keystat_link', true );
    $keystat_index = get_post_meta( $post->ID, 'keystat_index', true );   
    
    $dotstat_query = new WP_Query;
    // Sewlect all tax_dotstat
    $tax_dotstat_query = $dotstat_query->query( array(
        'post_type' => 'tax_dotstat',
        'posts_per_page' => -1
    ) );

    $options = $output = '';
    // parse selected posts
    foreach( $tax_dotstat_query as $pst ){
        $options .= '<option value="'.$pst->ID.'" '.checkSelected( intval($chart_entity), intval($pst->ID) ).' >'.esc_html( $pst->post_title ).'</option>';	
    }
    
    if($post->ID){
        $output .= '<div class="dotstat_shortcode">'
            .'<span>Shortcode to use on page:</span>'
            .'<code>[dotstat display="'.$post->ID.'"]</code>'
            .'</div>';
    }

    // Output the field     
    
    $output .= '<div class="dotstat_wraper">';
    $output .= '<div class="dotstat_general_settings">';
    $output .= '<div class="dotstat_row"><h3>1. General Settings</h3></div>';
    // $output .= '<div class="dotstat_grid_row">';
    $output .= '<div class="dotstat_row">
        <h4>1.1. Select display type</h4>
        <select class="dotstat_display_type_output" name="type_display">                       
            <option '.checkSelected($dotstat_type_display, "Chart").' value="Chart">Chart</option>
            <option '.checkSelected($dotstat_type_display, "Table").'  value="Table">Table</option>
            <option '.checkSelected($dotstat_type_display, "KeyStat").'  value="KeyStat">KeyStat</option>
        </select>
    </div>';
    $output .= '<div class="dotstat_row">
        <h4>1.2. Selection data entity</h4>
        <select  name="chart_entity" class="form-select custom_action_block_select" data-live-search="true" >
            <option value="">- No Entity Selected -</option>
            '. $options.'
        </select> 
    </div>';

    $output .= '</div>';
        
        
    $output .= '<div class="dotstat_chart_block">';

    $output .= '<div class="dotstat_row"><h3>2. Chart Settings</h3></div>';
    $output .= '<div class="dotstat_grid_row">';
    $output .= '<div><h4>Chart title</h4>
            <input type="text" name="chart_title" value="' .  $chart_title  . '"  class="widefat">
        </div>';
    $output .= '<div><h4>Chart subtitle</h4>
            <input type="text" name="chart_subtitle" value="' .  $chart_subtitle  . '"  class="widefat">
        </div>';
    $output .= '</div>';

    $output .= '<div class="dotstat_grid_row">';

    $output .= '<div class=""><h4>Stat Link to source</h4>
            <input type="text" name="request_url" value="' . $request_url . '" >
            <p>Leave empty to use default link from Data Entity</p>
        </div>';

    $output .= '<div><h4>HighCharts Configuration Template</h4>
            <select  name="chart_template" class="form-select">
                <option value="">- No Visualisation -</option>';
    foreach ($GLOBALS['dotstat_chart_templates'] as $tpl) {
        $name = preg_replace('/(?<=[a-z])[A-Z]|[A-Z](?=[a-z])/', " $0", $tpl);
        $output .= '<option '.checkSelected($chart_template, $tpl).' value="'.$tpl.'">'.$name.'</option>';
    }
    $output .= '</select>
        </div>';
    $output .= '</div>';

    $output .= '<div class="dotstat_row"><h4>HighCharts Configuration (JSON Object)</h4>
            <textarea rows="8" cols="65" type="text" name="charts_configuration" >' . $charts_configuration . '</textarea>
        </div>';

    $output .= '</div>';

    $output .= '<div class="dotstat_table_block" style="display: none;">';

    $output .= '<div class="dotstat_row"><h3>2. Table Settings</h3></div>';

    $output .= '<div class="dotstat_grid_row">';
    $output .= '<div><h4>Table Title</h4>
            <input type="text" name="dot_stat_table_title" value="' .  $dot_stat_table_title  . '" >
        </div>';
    $output .= '<div><h4>Table Column Name</h4>
            <input type="text" name="dot_stat_table_column_name" value="' . $dot_stat_table_column_name . '"  >
        </div>';
    $output .= '</div>';


    $output .= '<div class="dotstat_grid_row">';
    $output .= '<div><h4>Table Header Css Class</h4>
            <input type="text" name="dot_stat_table_header_css" value="' .  $dot_stat_table_header_css  . '" >
        </div>';
    $output .= '<div><h4>Table Link</h4>
            <input type="text" name="dot_stat_table_link" value="' . $dot_stat_table_link . '"  >
        </div>';
    $output .= '</div>';


    $output .= '<div class="dotstat_grid_row">';

    $output .= '<div><h4>Data  Format</h4>
            <select  name="dot_stat_table_format" >
                <option '.checkSelected($dot_stat_table_format, "0").' value="0">Default</option>
                <option '.checkSelected($dot_stat_table_format, "1").' value="1">Number</option>
                <option '.checkSelected($dot_stat_table_format, "2").' value="2">Percentage</option>
                <option '.checkSelected($dot_stat_table_format, "3").' value="3">Rate</option>
                <option '.checkSelected($dot_stat_table_format, "4").' value="4">Yes/No</option>
                
            </select>
        </div>';
    $output .= '</div>';

    $output .= '</div>';

    $output .= '<div class="dotstat_keystat_block" style="display: none;">';

    $output .= '<div class="dotstat_row"><h3>2. KeyStat Settings</h3></div>';

    $output .= '<div class="dotstat_grid_row">';
    $output .= '<div><h4>KeyStat Label</h4>
            <input type="text" name="keystat_label" value="' .  $keystat_label  . '" >
        </div>';
    $output .= '<div><h4>KeyStat Format</h4>
            <select  name="keystat_format" >
                <option '.checkSelected($keystat_format, "1").' value="1">KeyStat format (number)</option>
                <option '.checkSelected($keystat_format, "2").' value="2">KeyStat format (rate)</option>
                <option '.checkSelected($keystat_format, "3").' value="3">KeyStat format (currency $)</option>                          
            </select>
        </div>';           
    $output .= '</div>';


    $output .= '<div class="dotstat_grid_row">';
    $output .= '<div><h4>KeyStat Icon</h4>
        <select  name="keystat_icon" >
            <option value="">- No icon -</option>';
    asort($iconset);
    foreach ($iconset as $icon => $label) {
        $output .= '<option '.checkSelected($keystat_icon, $icon).' value="'.$icon.'">'.$label.'</option>';
    }
    $output .= '</select>
        </div>';  

    $output .= '<div><h4>KeyStat Link</h4>
            <input type="text" name="keystat_link" value="' . $keystat_link . '"  >
        </div>';
    $output .= '</div>';

    $output .= '<div class="dotstat_row">';
    $output .= '<div><h4>KeyStat Indicator</h4>
        <select name="keystat_index">';

    $data = dotstat_list_datasets($chart_entity);
    if (is_array($data)) {
      foreach ($data as $key => $val) {
        $output .= '<option value="'.$key.'" '.checkSelected($keystat_index, $key).'>'.$val.'</option>';
      }
    } else {
      $output .= '<option value="">Please select data entity</option>';
    }

    $output .= '</select>
    </div>';
    $output .= '</div>';            

    $output .= '</div>';

    $output .= '</div>';

    echo $output;
}

function checkSelected($a, $b){
    if($a === $b){
        return 'selected';
    }
    return '';
}

function dotstat_list_datasets( $nid ) {

  $node = get_post( $nid );

  if( ! $node || $node->post_type != 'tax_dotstat'  ){
    return '404';
  }

  $jstring = trim( get_post_meta( $node->ID, 'tax_stored_parsed_data', true ) );
      
  if (empty($jstring)) {
    return '502';
  }
    
  $parsed = json_decode( $jstring );
  
  $data = [];
  foreach ($parsed->data as $sid => $sobj) {
    $data[ $sid ] = $sobj->name??$sid;
  }
  return $data;
 }


if ( ! function_exists('mtbx_save_dotstat_meta') ) {
/**
 * Save the metabox data
 */
function mtbx_save_dotstat_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( ! isset( $_POST['chart_title'] ) || ! isset( $_POST['request_url'] ) || ! wp_verify_nonce( $_POST['dotstat_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    // Now that we're authenticated, time to save the data.
    // This sanitizes the data from the field and saves it into an array $events_meta.
    $dotstat_meta['chart_title'] = esc_textarea( $_POST['chart_title'] );            
    $dotstat_meta['chart_subtitle'] = esc_textarea( $_POST['chart_subtitle'] );
    $dotstat_meta['chart_entity'] = esc_textarea( $_POST['chart_entity'] );
    $dotstat_meta['chart_template'] = esc_textarea( $_POST['chart_template'] );
    $dotstat_meta['type_display'] = esc_textarea( $_POST['type_display'] );

    // $dotstat_meta['data_depth'] = $_POST['data_depth'] ;

    $dotstat_meta['request_url'] =  $_POST['request_url'];
    $dotstat_meta['charts_configuration'] =  $_POST['charts_configuration'] ;

    // Table settings
    $dotstat_meta['dot_stat_table_title'] = esc_textarea( $_POST['dot_stat_table_title'] );
    $dotstat_meta['dot_stat_table_column_name'] = esc_textarea( $_POST['dot_stat_table_column_name'] );
    $dotstat_meta['dot_stat_table_header_css'] = esc_textarea( $_POST['dot_stat_table_header_css'] );            
    $dotstat_meta['dot_stat_table_link'] =  $_POST['dot_stat_table_link'];
    // $dotstat_meta['table_label'] = esc_textarea( $_POST['table_label'] );
    $dotstat_meta['dot_stat_table_format'] = esc_textarea( $_POST['dot_stat_table_format'] );

    // Keystat settings
    $dotstat_meta['keystat_label'] =  $_POST['keystat_label'];
    $dotstat_meta['keystat_format'] = esc_textarea( $_POST['keystat_format'] );
    $dotstat_meta['keystat_icon'] = esc_textarea( $_POST['keystat_icon'] );
    $dotstat_meta['keystat_link'] = $_POST['keystat_link'] ;
    $dotstat_meta['keystat_index'] = esc_textarea( $_POST['keystat_index'] );
                

    // Cycle through the $dotstat_meta array.
    // Note, in this example we just have one item, but this is helpful if you have multiple.
    foreach ( $dotstat_meta as $key => $value ) :

        // Don't store custom data twice
        if ( 'revision' === $post->post_type ) {
            return;
        }

        if ( get_post_meta( $post_id, $key, false ) ) {
            // If the custom field already has a value, update it.
            update_post_meta( $post_id, $key, $value );
        } else {
            // If the custom field doesn't have a value, add it.
            add_post_meta( $post_id, $key, $value);
        }

        // if ( ! $value ) {
        // 	// Delete the meta key if there's no value
        // 	delete_post_meta( $post_id, $key );
        // }

    endforeach;

}
add_action( 'save_post', 'mtbx_save_dotstat_meta', 1, 2 );

} // end if function exists


