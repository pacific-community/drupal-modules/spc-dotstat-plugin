<?php
if ( ! function_exists('dotstat_taxpost_type') ) {

  // Register Custom Post Type
  function dotstat_taxpost_type() {

    $labels = array(
      'name'                  => _x( '.STAT Dataset', 'Post Type General Name', 'dotstat' ),
      'singular_name'         => _x( '.STAT Dataset', 'Post Type Singular Name', 'dotstat' ),
      'menu_name'             => __( '.STAT Dataset', 'dotstat' ),
      'name_admin_bar'        => __( '.STAT Dataset', 'dotstat' ),
      'archives'              => __( '.STAT Dataset', 'dotstat' ),
      'attributes'            => __( '.STAT Dataset Attributes', 'dotstat' ),
      'parent_item_colon'     => __( '', 'dotstat' ),
      'all_items'             => __( 'All .STAT Dataset', 'dotstat' ),
      'add_new_item'          => __( 'New .STAT Dataset', 'dotstat' ),
      'add_new'               => __( 'Add new', 'dotstat' ),
      'new_item'              => __( 'New .STAT Dataset', 'dotstat' ),
      'edit_item'             => __( 'Edit .STAT Dataset', 'dotstat' ),
      'update_item'           => __( 'Update .STAT Dataset', 'dotstat' ),
      'view_item'             => __( 'View .STAT Dataset', 'dotstat' ),
      'view_items'            => __( 'View .STAT Dataset', 'dotstat' ),
      'search_items'          => __( 'Search .STAT Dataset', 'dotstat' ),
      'not_found'             => __( 'Not found any .STAT Dataset', 'dotstat' ),
      'not_found_in_trash'    => __( 'Not found any .STAT Dataset', 'dotstat' ),
      'featured_image'        => __( 'Featured image of .STAT Dataset', 'dotstat' ),
      'set_featured_image'    => __( 'Set featured image for .STAT Dataset', 'dotstat' ),
      'remove_featured_image' => __( 'Remove featured image', 'dotstat' ),
      'use_featured_image'    => __( 'Set featured image', 'dotstat' ),
      'insert_into_item'      => __( 'Add to .STAT Dataset', 'dotstat' ),
      'uploaded_to_this_item' => __( 'Uploaded to this .STAT Dataset', 'dotstat' ),
      'items_list'            => __( 'List .STAT Dataset', 'dotstat' ),
      'items_list_navigation' => __( '', 'dotstat' ),
      'filter_items_list'     => __( '', 'dotstat' ),
    );

    $args = array(
      'label'                 => __( 'tax_dotstat', 'dotstat' ),
      'description'           => __( '.STAT Dataset post type', 'dotstat' ),
      'labels'                => $labels,
      'supports'              => array( 'title'),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => false,
      'show_in_nav_menus'     => false,
      'show_in_rest'          => false,
      'can_export'            => false,
      'has_archive'           => false,
      'exclude_from_search'   => false,
      'publicly_queryable'    => false,
      'capability_type'       => 'page',
      'menu_icon'             => 'dashicons-layout'
    );
    register_post_type( 'tax_dotstat', $args );

  }

  add_action( 'init', 'dotstat_taxpost_type', 0 );


  if(!function_exists('add_taxdotstat_metaboxes')) {

    add_action( 'add_meta_boxes', 'add_taxdotstat_metaboxes' );

    function add_taxdotstat_metaboxes(){
      add_meta_box(
        'taxdotstat_meta',
        'Chart fields',
        'taxdotstat_meta',
        'tax_dotstat',
        'normal',
        'default'
      ); 
    }
  }

}


/**
* Output the HTML for the metabox.
*/
function taxdotstat_meta() {

  global $post;

  // Nonce field to validate form request came from current site
  wp_nonce_field( basename( __FILE__ ), 'taxdotstat_fields' );

  // Get the location data if it's already been entered
  $request_url = get_post_meta( $post->ID, 'request_url', true );
  $taxparser_config = get_post_meta( $post->ID, 'taxparser_config', true );
  $default_link = get_post_meta( $post->ID, 'default_link', true );
  $stored_data = get_post_meta( $post->ID, 'tax_stored_parsed_data', true );

  // default parser config (template)
  if (empty($taxparser_config)) {
    $taxparser_config = "{\n  \"series\": [],"
      ."\n  \"data\": {"
      ."\n    \"index\": \"dim[X].id\","
      ."\n    \"name\": \"dim[Y].name\","
      ."\n    \"value\": \"value\""
      ."\n  }\n}";
  }

  // Output the field

  $output = '<div class="dotstat_wraper">';

  $output .= '<div class="dotstat_row"><h4>PDH.STAT API Query URI <sup>*</sup></h4>
        <input type="text" class="taxcpt_request_url" name="request_url" value="' . $request_url  . '" required >
        <div class="tax_request_dotstat">
            <div class="button taxcpt_dotstat_check_link"><span class="dashicons dashicons-controls-play"></span> Check query &amp; Parse response</div>
            <span class="taxtcp_loader_block"><span class="taxtcp_loader_wraper"><span class="taxtcp_loader"></span>Sending query...</span></span>
        </div>
        
        <div class="tax_request_dotstat-result" style="display: none;">
            <h3 class="tax_request_dotstat-msg" ></h3>
            <div class="request_dotstat-resp-wraper">
                <h3 class="open_request_dotstat-header"><span class="dotstat-header-arrow"></span>DIMENSIONS</h3>
                <div class="request_dotstat-data-box tax_request_dotstat-dimension"></div>
            </div>
            <div class="request_dotstat-resp-wraper">
                <h3 class="open_request_dotstat-header"><span class="dotstat-header-arrow"></span>ATTRIBUTES</h3>
                <div class="request_dotstat-data-box tax_request_dotstat-attributes"></div>
            </div>
            <div class="request_dotstat-resp-wraper">
                <h3 class="open_request_dotstat-header"><span class="dotstat-header-arrow"></span>FULL RESPONSE (JSON)</h3>
                <div class="request_dotstat-data-box tax_request_dotstat-resp"><pre></pre></div>
            </div>
        </div>
    </div>';

  $output .= '<div class="dotstat_row"><h4>Parser configuration <sup>*</sup></h4>
                  <textarea class="taxparser_config" name="taxparser_config" cols="60" rows="8">' .  $taxparser_config . '</textarea>
              </div>';

  $output .= '<div class="dotstat_row">
                  <div style="margin-top:1em">
                    <div class="button taxcpt_refresh_store_data" data-pid="'.$post->ID.'"><span class="dashicons dashicons-controls-play"></span> Query, Parse &amp; Store data</div>
                    <span class="taxtcp_loader_block"><span class="taxtcp_loader_wraper"><span class="taxtcp_loader"></span>Sending query...</span></span>
                  </div>
                  <h3 class="tax_request_store_set" style="dispaly: none;" ></h3>
                  
              </div>';

  $output .= '<div class="dotstat_row"><h4>Stored dataset</h4>
                  <div class="tax_stored_parsed_data"><pre></pre></div>
              </div>';

  $output .= '<div class="dotstat_row"><h4>Source Link</h4>
                  <input type="text" name="default_link" value="' .  $default_link . '" >
              </div>';

  $output .= '</div>';

  echo $output;
}


if ( ! function_exists('dtmt_save_taxdotstat_meta') ) {
  /**
   * Save the metabox data
   */
  function dtmt_save_taxdotstat_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( ! isset( $_POST['request_url'] ) || ! isset( $_POST['default_link'] ) || ! wp_verify_nonce( $_POST['taxdotstat_fields'], basename(__FILE__) ) ) {
      return $post_id;
    }

    // Now that we're authenticated, time to save the data.
    // This sanitizes the data from the field and saves it into an array $events_meta.
    $dotstat_meta['request_url'] =  $_POST['request_url'] ;
    $dotstat_meta['default_link'] =  $_POST['default_link'] ;
    $dotstat_meta['taxparser_config'] = $_POST['taxparser_config'] ;
    
    // Cycle through the $dotstat_meta array.
    // Note, in this example we just have one item, but this is helpful if you have multiple.
    foreach ( $dotstat_meta as $key => $value ) :

      // Don't store custom data twice
      if ( 'revision' === $post->post_type ) {
          return;
      }

      if ( get_post_meta( $post_id, $key, false ) ) {
        // If the custom field already has a value, update it.
        update_post_meta( $post_id, $key, $value );
      } else {
        // If the custom field doesn't have a value, add it.
        add_post_meta( $post_id, $key, $value);
      }

      // if ( ! $value ) {
      // 	// Delete the meta key if there's no value
      // 	delete_post_meta( $post_id, $key );
      // }

    endforeach;

  }
  add_action( 'save_post', 'dtmt_save_taxdotstat_meta', 1, 2 );

}

