 (function($) {
    var context = null;

      DotStatChart = function(id, categories, otpl, opts, title, subtitle, link) {

        this.debug = (opts && opts.debug)?true:((otpl && otpl.debug)?true:false);

        /**
         * Debug method
         */
        this.dbg = function(s) {
          if (this.debug) { console.log(s); }
        }

        /**
         * Constructor (continue)
         */
        this.id = id;
        this.categories = categories;
        this.dbg('CATEGORIES = ');
        this.dbg(this.categories);
        this.rawdata = false;
        this.drilldata = false;
        this.animating = false;

        // define common / default options
        this.options = {
          title: {
            useHTML: true,
            text: title?title:''
          },
          subtitle: {
            useHTML: (subtitle && link)?true:false,
            text: subtitle?(link?('<a href="'+link+'" target="_blank">'+subtitle+'</a>'):subtitle):''
          },
          exporting: {
            menuItemDefinitions: {
              showDatTable: {
                text: 'Show Data Table',
                onclick: function() {
                  $('#'+id+'-modal .modal-body', context).html(this.getTable());
                  if ( $.isFunction($.fn.modal) ) {
                    $('#'+id+'-modal', context).modal('show');
                  }
                }
              },
              sourceLink: {
                text: 'View Data in PDH.stat',
                onclick: function() {
                  window.open(link.replace(/&amp;/g, "&"), '_blank');
                }
              }
            },
            showTable: false
          },
          dotStatChartOptions: {
            rawDataType: ''
          }
        };

        // this.dbg(this.options);

        // Loading icon (animated SVG)
        this.loader = '<svg viewBox="0 0 135 140" xmlns="http://www.w3.org/2000/svg" class="highcharts-loader"><rect x="0" y="5" width="15" height="140" rx="6"><animate attributeName="height" begin="0s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="30" y="50" width="15" height="100" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="60" y="25" width="15" height="120" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="90;80;70;60;45;30;140;120;120;110;100" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="55;65;75;85;100;115;5;25;25;35;45" calcMode="linear" repeatCount="indefinite" /></rect><rect x="90" y="30" width="15" height="120" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="120" y="70" width="15" height="80" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect></svg>';

        // extend options with template settings
        this.dbg('TEMPLATE = ');
        this.dbg(otpl);
        if (otpl) {
          this.dbg('MERGING TEMPLATE OPTIONS...');
          $.extend(true, this.options, otpl);
          this.dbg(this.options);
        }

        // extend options with block specifics
        this.dbg('EXTENDING OPTIONS...');
        $.extend(true, this.options, opts);

        // check exporting options
        if (!this.options.exporting.buttons) {
          this.options.exporting.buttons = {
            contextButton: {
              menuItems: ['viewFullscreen','printChart','showDatTable','separator','downloadPNG','downloadPDF','downloadSVG']
            }
          };
          // 'downloadCSV',
        }

        // do we have a link ?
        if (link) {
          this.options.exporting.buttons.contextButton.menuItems.unshift('sourceLink');
        }

        // @todo check options are valid
        this.dbg(this.options);

        // Add categories
        if  (this.options.xAxis) {
          if (typeof this.categories == "object") {
            for (var j=0; j < this.options.xAxis.length; j++) {
              this.options.xAxis[j].categories = this.categories;
            }
          } else {
            for (var j=0; j < this.options.xAxis.length; j++) {
              this.options.xAxis[j].type = this.categories;
            }
          }
        }

        /**
         * Initialize HTML structure (DOM)
         */
        this.initHtml = function() {
          var $d = $('#'+this.id);
          if (!$d[0]) {
            alert('ERR: Target div #'+this.id+' not found');
            return false;
          }
          var tabs = this.options.showTable || this.debug; // use tabs only if needed (showing table or debug mode ON)
          var html = '';
          if (tabs) {
            html += '<div class="sdd-chart-panel">'
              + '<ul class="nav nav-tabs" id="dotstat-sdg451-tabs" role="tablist">'
              + '<li class="nav-item active">'
              + '<a class="nav-link" data-toggle="tab" href="#'+this.id+'-chart" role="tab" aria-controls="home" aria-selected="true">Chart</a>'
              + '</li>';
            if (this.options.showTable) {
              html += '<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#'+this.id+'-data-table" role="tab" aria-controls="profile" aria-selected="false">Data Table</a></li>';
            }
            if (this.debug) {
              html += '<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#'+this.id+'-jsondata" role="tab" aria-controls="contact" aria-selected="false">JSON</a></li>';
            }
            html += '</ul>'
              + '<div class="tab-content" id="'+this.id+'-content">'
              + '<div id="'+this.id+'-chart" class="highcharts-chart tab-pane fade active in">'
          } else {
            html += '<div id="'+this.id+'-chart" class="highcharts-chart">';
          }
          html += this.loader
            +'</div>';

          if (this.options.showTable) {
            html += '<div id="'+this.id+'-data-table" class="highcharts-data-table tab-pane fade">Loading...</div>';
          }
          if (this.debug) {    
            html += '<div id="'+this.id+'-jsondata" class="highcharts-jsondata tab-pane fade">...</div>';
          }
          if (tabs) {
            html += '</div></div>';
          }
          if (this.options.exporting && this.options.exporting.buttons.contextButton && this.options.exporting.buttons.contextButton.menuItems.indexOf('showDatTable') !== -1) {
            html += '<div class="modal fade" id="'+this.id+'-modal"><div class="modal-dialog modal-lg" role="document"><div class="modal-content">'
            +'<div class="modal-header"><div class="modal-title" id="dotstat-sdg451-modal-title">Data Table</div><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body highcharts-data-table"></div></div></div></div>';
          }
          $d.html(html);
          return true;
        }

        /**
         * Initial chart rendering
         * (this may be overloaded by templates)
         */
        this.render = function() {

          var obj = this;

          this.options.series = [ { data: this.rawdata } ];

          obj.dbg('Rendering...');
          obj.dbg(this.options);

          this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
            if (obj.options.showTable ) {
              $('#'+obj.id+'-data-table', context).html(this.getTable());
            }  
          });

        };

        /**
         * GO: this where it all starts
         * This is the public method to be called right after object has been instanciated
         * (might be overloaded e.g. adding drill down data)
         */
        this.go = function(data, drill) {
          this.rawdata = data;
          if (drill) {
            this.drilldata = drill;
          }
          if (this.initHtml()) {
            if (this.debug) {
              $('#'+this.id+'-jsondata', context).html(JSON.stringify(data)); //.beautifyJSON();
            }
            this.render();
          }
        };

        /**
         * Generate alternative sources dropdown
         */
        this.altsource = function(alternates, current) {
          this.dbg('Adding alternate sources select box');
          var html = '<select class="spc-chart-alt-source" onchange="'+this.id.replace('-','_')+'.altload(this.value)">';
          for( lbl in alternates ) {
            idx = alternates[lbl];
            html += '<option value="'+idx+'"';
            if (current == idx) {
              html += ' selected="selected"';
            }
            html += '>'+lbl+'</option>';
          }
          html += '</select>';
          var $sb = $('#'+this.id+' .spc-chart-switching-buddy', context);
          if ($sb[0]) {
            $sb.prepend(html);
          } else {
            $('#'+this.id, context).prepend('<div class="spc-chart-switching-buddy">'+html+'</div>');
          }
        }

        /*
         * Load alternate data (AJAX)
         */
        this.altload = function(nodeid) {
          var thischart = this;
          if (thischart.animating) {
            window.clearInterval(thischart.animating);
            if (thischart.iplay) {
              thischart.iplay.removeClass('pause');
            }
            thischart.animating = false;
          }

          Drupal.ajax({
              url: Drupal.url('spc_dot_stat_chart/ajax/'+nodeid+'/'+this.options.dotStatChartOptions.rawDataType),
          })
          .execute()
          .done(function(response) {
              if (response.status == 0) {
                if (response.label) {
                  thischart.chart.setTitle({text: response.label});
                }
                // update raw data
                thischart.update(response.data, response.options);
              }
          });
        }

        /**
         * Update data in chart (single series)
         */
        this.update = function(data, options) {
          if (options) {
            this.chart.series[0].update(options, false);
          }
          this.chart.series[0].setData(data);
        }

        return this;

      }

})(jQuery);