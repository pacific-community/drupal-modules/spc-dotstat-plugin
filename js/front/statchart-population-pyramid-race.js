(function($) {
    var context = null;
      
      /**
       * Population Pyramid Race
       */  
      DotStatChartPopulationPyramidRace = function(id, categories, otpl, opts, title, subtitle, link) {

      	// inherit from super class
      	DotStatChart.call(this, id, categories, otpl, opts, title, subtitle, link);

       /**
        * Initial chart rendering
        * (overloading default method)
        */
        this.render = function() {

          var obj = this;

          var maxidx = 0;
          var maxval = 0;
          this.datasets = [];
          for (var kn in this.rawdata) {
            this.datasets.push(kn);
            for (var kx in this.rawdata[kn]['Male']) {
              maxval = Math.max(maxval, this.rawdata[kn]['Male'][kx], this.rawdata[kn]['Female'][kx]);
            }
            maxidx = kn;
          }
          if (!(this.options.yAxis.min || this.options.yAxis.max)) {
            this.options.yAxis.min = 0-maxval;
            this.options.yAxis.max = maxval;
          }
          var minidx = this.datasets[0];
          this.dbg('RACING PYRAMID:');
          this.dbg(this.datasets);

          var curidx = minidx;
          if (this.options.dotStatChartOptions.defaultSet) {
            if (this.options.dotStatChartOptions.defaultSet == 'year') {
              var dnow = new Date();
              curidx = dnow.getFullYear();
            } else {
              curidx = this.options.dotStatChartOptions.defaultSet;
            }
          }

          this.options.series = [ {
            name: 'Male',
            data: $.map(this.rawdata[curidx]['Male'], function(a) { return 0 - a ; }),
            color: '#9cf'
          }, {
            name: 'Female',
            // data: [...this.rawdata[first]['Female']],
            data: $.map(this.rawdata[curidx]['Female'], function(a) { return a; }),
            color: '#f9c'
          } ];

          var htmlrange = '<div class="spc-play-controls">'
            + '<div class="spc-play-button-div">'
            + '<button class="spc-play-pause-button" title="play"></button>'
            + '</div>'
            + '<div class="spc-play-range-div">'
            + '<input class="spc-play-range" type="range" value="'+curidx+'" min="'+minidx+'" max="'+maxidx+'" />'
            + '</div>'
            + '<div class="spc-play-name-div">'
            + '<span class="spc-play-name">'+curidx+'</span>'
            + '</div>'
            + '</div>';

          obj.dbg('Rendering...');
          obj.dbg(this.options);

          this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
            if (obj.options.showTable ) {
              $('#'+obj.id+'-data-table', context).html(this.getTable());
            }  
          });

          $('#'+this.id, context).append('<div class="spc-chart-racing-buddy">'+htmlrange+'</div>'); 

          this.irange = $('#'+this.id+' .spc-play-range', context);
          this.iplay = $('#'+this.id+' .spc-play-pause-button', context);
          this.iname = $('#'+this.id+' .spc-play-name', context);
          this.showtotal(this.irange.val());

          this.irange.on('click', function() {
            if (obj.animating) {
              window.clearInterval(obj.animating);
              obj.iplay.removeClass('pause');
              obj.animating = false;
            }
            var val = this.value;
            obj.iname.html(val);
            obj.chart.series[0].setData($.map( obj.rawdata[val]['Male'], function(a) { return 0 - a ; }));
            obj.chart.series[1].setData(obj.rawdata[val]['Female']);
            obj.showtotal(val);
          });

          this.iplay.on('click', function() {
            if (obj.animating) {
              window.clearInterval(obj.animating);
              obj.iplay.removeClass('pause');
              obj.animating = false;
            } else {
              obj.iplay.addClass('pause');
              obj.animating = window.setInterval(function() {
                var val = parseInt(obj.irange.val());
                var om = parseInt(obj.irange.attr('max'));
                if (val >= om) {
                  val = parseInt(obj.irange.attr('min'));
                } else {
                  val++;
                }
                obj.irange.val(val);
                obj.iname.html(val);
                obj.chart.series[0].setData($.map( obj.rawdata[val]['Male'], function(a) { return 0 - a ; }));
                obj.chart.series[1].setData(obj.rawdata[val]['Female']);
                obj.showtotal(val);
              }, obj.options.dotStatChartOptions.speed);
            }
          });
        } // end render

        /**
         * Switch Year (Population Pyramid multi specific)
         */
        this.switch = function(val) {
          if (this.animating) {
            window.clearInterval(this.animating);
            $('#'+this.id+' .spc-chart-switching-buddy button', context).removeClass('pause');
            this.animating = false;
          }
          this.chart.series[0].setData($.map( this.rawdata[val]['Male'], function(a) { return 0 - a ; }));
          this.chart.series[1].setData(this.rawdata[val]['Female']);
        }


        this.update = function(data, options) {
          if (options) {
            this.chart.series[0].update(options, false);
          }
          // update raw data and switch to default set
          this.rawdata = data;
          this.irange.val(this.options.dotStatChartOptions.defaultSet);
          this.iname.html(this.options.dotStatChartOptions.defaultSet);
          var maxval = this.calcmax();
          var opts = {
            yAxis: {
              min: 0-maxval,
              max: maxval
            }
          }
          this.chart.update(opts, true);
          this.switch(this.options.dotStatChartOptions.defaultSet);
          this.showtotal(this.options.dotStatChartOptions.defaultSet);
        }

        /**
         * calculate population pyramid max value
         */
        this.calcmax = function() {
          // TODO: while looping through this, might want to check min/max range (and update input)
          var maxval = 0;
          for (var kn in this.rawdata) {
            for (var kx in this.rawdata[kn]['Male']) {
              maxval = Math.max(maxval, this.rawdata[kn]['Male'][kx], this.rawdata[kn]['Female'][kx]);
            }
          }
          return maxval;
        }

        /**
         * show total in chart subtitle
         */
        this.showtotal = function(val) {
          var total = 0;
          var i = 0;
          while (i < this.rawdata[val]['Male'].length) {
            total += this.rawdata[val]['Male'][i];
            total += this.rawdata[val]['Female'][i];
            i++;
          }
          this.chart.setSubtitle({text: 'Total population ('+val+'): '
            + new Intl.NumberFormat(this.options.dotStatChartOptions.locale).format(total)});
        }
      }

})(jQuery);