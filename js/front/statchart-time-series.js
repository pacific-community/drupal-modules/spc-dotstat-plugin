(function($) {

  /**
   * Population Time series
   */
  DotStatChartTimeSeries = function(id, categories, otpl, opts, title, subtitle, link) {

  	// inherit from super class
  	DotStatChart.call(this, id, categories, otpl, opts, title, subtitle, link);

    	/**
       * Initial chart rendering
       * (overloading default method)
       */
    	this.render = function() {
        
        var obj = this;

        this.options.series = [];
        
        for (ir in this.rawdata) {
          var sdata = this.rawdata[ir];
          var sname = 'Series '+ir;
          if (sdata.name) {
            sname = sdata.name;
          } else if (sdata.data[0].name) {
            sname = sdata[0].name;
            for (ix in sdata) {
              delete sdata[ix]['name'];
            }
          }
          var row = {
            name: sname,
            data: sdata.data
          }
          // if rawdata series data has a date in x, convert it to Date
          row.data.map(item => {
            if (typeof(item.x) == 'object' && item.x.date) {
              item.x = new Date(item.x.date);
            }
          })
          if (this.options.dotStatChartOptions.colors && this.options.dotStatChartOptions.colors[ir]) {
            row.color = this.options.dotStatChartOptions.colors[ir];
          }
          if (this.options.dotStatChartOptions.symbol && this.options.dotStatChartOptions.symbol[ir]) {
            row.marker = {
              symbol: this.options.dotStatChartOptions.symbol[ir]
            }
          }
          this.options.series.push(row);
        }
            
        obj.dbg('Rendering...');
        obj.dbg(this.options);
        
        this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
          if (obj.options.showTable ) {
                  $('#'+obj.id+'-data-table', context).html(this.getTable());
          }  
        });

    	} // end render

    }

})(jQuery);