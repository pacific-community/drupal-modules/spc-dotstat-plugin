(function($) {     
  
      /**
       * Population Pyramid (Simple)
       */
      DotStatChartPopulationPyramid = function(id, categories, otpl, opts, title, subtitle, link) {

        // inherit from super class
        DotStatChart.call(this, id, categories, otpl, opts, title, subtitle, link);

        /**
         * Initial chart rendering
         * (overloading default method)
         */
        this.render = function() {

          var obj = this;

          var maxval = 0;
          for (var kx in this.rawdata['Male']) {
            maxval = Math.max(maxval, this.rawdata['Male'][kx], this.rawdata['Female'][kx]);
          }

          if (!(this.options.yAxis.min || this.options.yAxis.max)) {
            this.options.yAxis.min = 0-maxval;
            this.options.yAxis.max = maxval;
          }

          this.dbg('POPULATION PYRAMID:');

          this.options.series = [ {
            name: 'Male',
            data: $.map( this.rawdata['Male'], function(a) { return 0 - a ; }),
            color: '#9cf'
          }, {
            name: 'Female',
            data: $.map( this.rawdata['Female'], function(a) { return a ; }),
            color: '#f9c'
          } ];

          obj.dbg('Rendering...');
          obj.dbg(this.options);

          this.chart = Highcharts.chart(this.id+'-chart', this.options, function() {
            if (obj.options.showTable ) {
              $('#'+obj.id+'-data-table', context).html(this.getTable());
            }  
          });

        } // end render

        /**
         * Update dataset on Ajax
         */
        this.update = function(data, options) {
          if (options) {
            this.chart.update(options, false);
          }
          // update raw data and switch to default set
          this.rawdata = data;

          // calc min/max
          var maxval = 0;
          for (var kx in this.rawdata['Male']) {
            maxval = Math.max(maxval, this.rawdata['Male'][kx], this.rawdata['Female'][kx]);
          }
          var opts = {
            yAxis: {
              min: 0-maxval,
              max: maxval
            }
          }
          this.chart.update(opts, false);

          // render data    
          this.chart.series[0].setData($.map( this.rawdata['Male'], function(a) { return 0 - a ; }));
          this.chart.series[1].setData(this.rawdata['Female']);

        }
      }
     
})(jQuery);