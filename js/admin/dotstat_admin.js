jQuery(document).ready(function($){

    /**
     * Keystat display: Load datasets from entity
     */
    $(".dotstat_general_settings select[name=chart_entity]").change(function(){
        var $p = $('.dotstat_keystat_block');
        if ($p.is(':visible')) {
            var $o = $(this);
            var $s = $p.find('select[name=keystat_index]');
            $s.empty();
            $s.append('<option value="">Parsing #'+$o.val()+', please wait...</option>');
            var data = {
                action: 'dotstat_keystat_datasets',
                data_id: $o.val(),
                nonce_stat: dotstat_ajax.nonce
            };

            $.ajax({
                url: dotstat_ajax.url,
                type: 'POST',
                data: data,
                success: function (resp) {  
                    if (resp.code == 200) {
                        $s.empty();
                        for (var idx in resp.data) {
                            $s.append('<option value="'+idx+'">'+resp.data[idx]+'</option>');
                        }
                    } else {

                    }
                }
            });
        }
    });

    /**
     * Check PDH.stat API query
     */
    $(".taxcpt_dotstat_check_link").click(function(){

        var parent = $(this).parents('.dotstat_row');
        if(parent.hasClass('ajax_sent')) return;
        parent.addClass('ajax_sent');

        var request_url = $('input.taxcpt_request_url').val();
        var param = $('textarea.taxparser_config').val();
        var clearedParam = param.replace(/(\r\n|\n|\r)/gm, "");

        var data = {
			action: 'taxcpt_request_url',
            request_url: request_url,
            param: clearedParam,
            nonce_stat: dotstat_ajax.nonce,
		};

        $.ajax({
            url: dotstat_ajax.url,
            type: 'POST',
            data: data,
            success: function (data) {  
                $('.tax_request_dotstat-result').show();
                parent.removeClass('ajax_sent');
                $('.tax_request_dotstat-msg').text(data.message).css('color', data.code == 200?'#090':'#c11');
                if (data.code == 200) {
                    $('.tax_request_dotstat-resp pre').jsonViewer(data.resp);
                    $('.taxparser_config[name="tax_stored_parsed_data"]').val(data.parsed);
                
                    var table = '<table><thead><tr><th width="10%">Index</th><th width="30%">dim[x].id</th><th width="60%">dim[x].name</th></tr></thead><tbody>';
                    var index = 0;
                    if( data.structure_table && typeof(data.structure_table) == 'object' ){
                         for ( object in data.structure_table ) {
                            table += '<tr><td  width="15%">'+index+'</td><td  width="30%">'+object+'</td><td  width="55%">'+data.structure_table[object]+'</td></tr>';
                            index++;
                        }
                    } 
                    table += '</tbody></table>';  

                    $('.tax_request_dotstat-dimension').html(table);              

                    var atrTable = '<table><thead><tr><th width="10%">Index</th><th width="30%">ATTR[X].ID</th><th width="60%">ATTR[X].NAME</th></tr></thead><tbody>';
                    var index2 = 1;
                    if( data.attributes && typeof(data.attributes) == 'object' ){
                        for ( object in data.attributes ) {
                           atrTable += '<tr><td  width="15%">'+index2+'</td><td  width="30%">'+object+'</td><td  width="55%">'+data.attributes[object]+'</td></tr>';
                           index2++;
                       }
                    } 
                    atrTable += '</tbody></table>'; 
                    $('.tax_request_dotstat-attributes').html(atrTable);  
                    $('.request_dotstat-resp-wraper').show();
                } else {
                    $('.request_dotstat-resp-wraper').hide();
                }
              
            },
            error: function (data) {
                $('.tax_request_dotstat-result').show();
                parent.removeClass('ajax_sent');
                $('.tax_request_dotstat-msg').text('Something went wrong - please try again').css('color','#c11');      
                
            }
        });
    });


    /**
     * Query API, parse JSON and store parsed data
     */
    $(".taxcpt_refresh_store_data").click(function(){

        var parent = $(this).parents('.dotstat_row');
        if(parent.hasClass('ajax_sent')) return;
        parent.addClass('ajax_sent');
        
        var request_url = $('input.taxcpt_request_url').val();
        var param = $('textarea.taxparser_config').val();
        var postId = parseInt($(this).data('pid'));
        var clearedParam = param.replace(/(\r\n|\n|\r)/gm, ""); // why?
        var deflink = $('input[name="default_link"]').val();

        var data = {
			action: 'taxcpt_store_refresh',
            request_url: request_url,
            param: param,
            post_id: postId,
            default_link: deflink,
            nonce_stat: dotstat_ajax.nonce,
		};

        $.ajax({
            url: dotstat_ajax.url,
            type: 'POST',
            data: data,
            success: function (data) {  
                // $('.tax_request_dotstat-result').show();
                parent.removeClass('ajax_sent');
                $('.tax_request_store_set').text(data.message).css('color', data.code == 200?'#090':'#c11');;
                $('.tax_stored_parsed_data pre').jsonViewer(JSON.parse(data.data));
                $('.tax_request_store_set').show();                
              
            },
            error: function (data) {
                $('.tax_request_dotstat-result').show();
                parent.removeClass('ajax_sent');
                $('.tax_request_store_set').text('Something went wrong - please try again').css('color','#c11');
                $('.tax_request_store_set').show();     
                
            }
        });
    });


    /**
     * Response sliders
     */
    $('.open_request_dotstat-header').click(function(){
        var target = $(this);
        target.toggleClass( "dotstat-rotate-arrow" );

        if(target.hasClass("dotstat-rotate-arrow")){
            target.parents('.request_dotstat-resp-wraper').find('.request_dotstat-data-box').slideDown();
        } else {
            target.parents('.request_dotstat-resp-wraper').find('.request_dotstat-data-box').slideUp();
        }
    })


    /**
     * Display forms
     */
    function showDotstatTableSettings(){

        if($('.dotstat_display_type_output')){

            var selected = $('.dotstat_display_type_output').val() ? $('.dotstat_display_type_output').val().toLowerCase() : '';

            if( selected == 'table'){
                $('.dotstat_table_block').css('display', 'block');
                $('.dotstat_keystat_block').css('display', 'none');
                $('.dotstat_chart_block').css('display', 'none');

            } else if(selected == 'keystat'){

                $('.dotstat_table_block').css('display', 'none');
                $('.dotstat_keystat_block').css('display', 'block');
                $('.dotstat_chart_block').css('display', 'none');

            } else if(selected == 'chart'){

                $('.dotstat_table_block').css('display', 'none');
                $('.dotstat_keystat_block').css('display', 'none');
                $('.dotstat_chart_block').css('display', 'block');

            } else {

                $('.dotstat_table_block').css('display', 'none');
                $('.dotstat_keystat_block').css('display', 'none');
                $('.dotstat_chart_block').css('display', 'none');
            }
        }
    }

    $('.dotstat_display_type_output').change( showDotstatTableSettings);

    showDotstatTableSettings(); 

    // load "stored dataset" field with stored value
    $('.tax_stored_parsed_data pre').jsonViewer(JSON.parse(dotstat_ajax.stored_dataset));

});

jQuery(document).ready(function($){

    $('.custom_action_block_select').selectpicker();

});