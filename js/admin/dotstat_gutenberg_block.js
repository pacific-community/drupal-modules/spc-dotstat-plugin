( function( blocks, element, data ) {
    var el = element.createElement,
        registerBlockType = blocks.registerBlockType,
        withSelect = data.withSelect;
 
    registerBlockType( 'dostat-gutenberg/dostat-post-gutenberg', {
        title: '.STAT Display post',
        icon: 'analytics',
        category: 'common',
        attributes: {
            dotstat: {type: 'string', default:""},   
        },
        
        edit: withSelect( function( select ) {
            return {
                posts: select( 'core' ).getEntityRecords( 'postType', 'dotstat', { per_page: -1 } ),
            };
        } )( function( props ) {
            if ( ! props.posts ) {
                return 'Loading...';
            }
 
            if ( props.posts.length === 0 ) {
                return 'No posts';
            }            
            
            var childEl = [];
            function updateDotstat(event) {
                    props.setAttributes({dotstat: event.target.value});                                                                   
            }
                                
            props.posts.map(function(post){
                childEl.push( el("option", {value: post.id}, post.title.rendered));
            })

            setTimeout(setupSelect, 500);

            return el("div", {className: "custom_action_block"},
                         el("h5", {className: "custom_action_block"},"Select .STAT Display post"),
                        el(
                            'select',
                            { value: props.attributes.dotstat, className: 'custom_action_block_select selectpicker', 'data-live-search': "true", onChange: updateDotstat },
                            childEl
                        )
                    );
        } ),
           
    } );
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
) );
function setupSelect(){
    jQuery(document).ready(function($){

        $('.custom_action_block_select').selectpicker();

    });
}
