<?php
/*
Plugin Name: DotStat Block plugin
Plugin URI: 
Description: SPC Wordpress plugin to harvest data from Pacific data hub .Stat portal and display it as tables or charts using custom blocks.
Version: 1.2.4
Author: LinkDigital & SPC
Requires at least: 5.2
Requires PHP:      7.1
Author URI: https://linkdigital.com.au/
License: 
Text Domain: DotstatData_plugin
*/


// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define( 'DOTSTAT_VERSION', '1.1.0' );
define( 'DOTSTAT__MINIMUM_WP_VERSION', '5.9' );
define( 'DOTSTAT__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


register_activation_hook( __FILE__, 'dotstat_plugin_activate' );

add_action( 'wp_initialize_site', 'dotstat_plugin_new_blog', 10, 2 );


//   ability to activate plugin for each site

function dotstat_plugin_activate( $network_wide ){

		// require_once ABSPATH . 'wp-admin/includes/upgrade.php'; 

	if( is_multisite() && $network_wide ){ 

		global $wpdb;

		foreach( $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs") as $blog_id ){

			switch_to_blog($blog_id);

			_activate_plugin_for_site();

			restore_current_blog();
		} 

	} else {
		_activate_plugin_for_site();
	}

}


function _activate_plugin_for_site(){

	wp_clear_scheduled_hook('dotstat_daily_request_and_update');
	wp_schedule_event( time(), 'daily', 'dotstat_daily_request_and_update');
	
	add_option( 'Activated_Dotstatdata', true );

}


// Deactivation site

register_deactivation_hook( __FILE__, 'dotstat_deactivation_plugin' );

function dotstat_deactivation_plugin( $network_wide ){
	// require_once ABSPATH . 'wp-admin/includes/upgrade.php'; 

	if( is_multisite() && $network_wide ){ 

		global $wpdb;

		foreach( $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs") as $blog_id ){

			switch_to_blog($blog_id);

			wp_clear_scheduled_hook('dotstat_daily_request_and_update');

			restore_current_blog();
		} 

	} else {
		wp_clear_scheduled_hook('dotstat_daily_request_and_update');
	}
}

// Init new blog calback

function dotstat_plugin_new_blog( $new_site, $args ){

	
	if( is_plugin_active_for_network( DOTSTAT__PLUGIN_DIR . 'dotstatdata.php' ) ) {

		switch_to_blog( $new_site->site_id );
		
		_activate_plugin_for_site();

		restore_current_blog();
	} 

}

$GLOBALS['dotstat_chart_templates'] = array(
	'Columns',
	'Drilldown',
	'Lollipop',
	'Pie',
	// 'PieSummary',
	'PopulationPyramid',
	// 'PopulationPyramidMulti',
	'PopulationPyramidRace',
	'TimeSeries'
);

require_once( DOTSTAT__PLUGIN_DIR . 'parser/cpt.dotstat.php' );
require_once( DOTSTAT__PLUGIN_DIR . 'parser/taxcpt.dotstat.php' );
require_once( DOTSTAT__PLUGIN_DIR . 'parser/statupdate.dotstat.php' );
require_once( DOTSTAT__PLUGIN_DIR . 'parser/shortcode.dotstat.php' );
require_once( DOTSTAT__PLUGIN_DIR . 'admin/admin_page.dotstat.php' );

add_action( 'admin_enqueue_scripts', 'dotstat_admin_styles' );

function dotstat_admin_styles($hook) {
	
	global $post;	
	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		if($post->post_type == 'post' || $post->post_type == 'page' || $post->post_type == 'dotstat'){ 
			wp_enqueue_script( 'bootstrap_js_dotstat', plugins_url('js/admin/bootstrap.min.js', __FILE__),  array(), true );
			wp_enqueue_style( 'bootstrap_css_dotstat', plugins_url('css/admin/bootstrap.min.css', __FILE__),  array());	
			wp_enqueue_style( 'select_css_dotstat', plugins_url('css/admin/bootstarp-select.min.css', __FILE__), array());
			wp_enqueue_script( 'select_js_dotstat', plugins_url('js/admin/bootstrap-select.js', __FILE__),  array(), true );
		} 		
	}   	

	wp_enqueue_style( 'admin_css_json_viewer', plugins_url('css/admin/jquery.json-viewer.css', __FILE__) );
	wp_enqueue_script( 'admin_js_json_viewer', plugins_url('js/admin/jquery.json-viewer.js', __FILE__),  array(), true );
	wp_enqueue_style( 'admin_css_dotstat', plugins_url('css/admin/admin-style-dotstat.css', __FILE__) );
	wp_enqueue_script( 'admin_js_dotstat', plugins_url('js/admin/dotstat_admin.js', __FILE__), array('jquery'), true );
		
	wp_localize_script( 'admin_js_dotstat', 'dotstat_ajax', 
		array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('chart_refresh_nonce'),
			'stored_dataset' => get_post_meta( $post->ID, 'tax_stored_parsed_data', true )
		)
	); 
}    


add_action( 'wp_enqueue_scripts', 'dotstat_tcp_script' );

function dotstat_tcp_script(){
	wp_enqueue_script( 'dotstat_bootsrap_js', plugins_url('js/front/bootstrap.min.js', __FILE__), array(), false, false );
	wp_enqueue_script( 'dotstat_main_js', plugins_url('js/front/dotstat_main.js', __FILE__), array(), false, false );
	wp_enqueue_script( 'dotstat_tcp_highchart', plugins_url('js/front/highcharts.js', __FILE__), array(), false, true );
	wp_enqueue_script( 'dotstat_tcp_js_map', plugins_url('js/front/highcharts.js.map', __FILE__), array('dotstat_highchart'), false, true );
	wp_enqueue_script( 'dotstat_tcp_drilldown', plugins_url('js/front/drilldown.js', __FILE__), array('dotstat_tcp_highchart'), false, true );
	wp_enqueue_script( 'dotstat_tcp_highcharts-more', plugins_url('js/front/highcharts-more.js', __FILE__), array('dotstat_tcp_highchart'), false, true );
	wp_enqueue_script( 'dotstat_tcp_dumbbell', plugins_url('js/front/dumbbell.js', __FILE__), array('dotstat_tcp_highcharts-more'), false, true );

	wp_register_script( 'dotstat_statchart', plugins_url('js/front/statchart.js', __FILE__), array('jquery'), true, true);
	wp_register_script( 'dotstat_statchart_drilldown', plugins_url('js/front/statchart-drilldown.js', __FILE__), array('dotstat_statchart'), true, true);
	wp_register_script( 'dotstat_statchart_population_pyramid', plugins_url('js/front/statchart-population-pyramid.js', __FILE__), array('dotstat_statchart'), true, true);
	wp_register_script( 'dotstat_statchart_population_pyramid_race', plugins_url('js/front/statchart-population-pyramid-race.js', __FILE__), array('dotstat_statchart'), true, true);
	wp_register_script( 'dotstat_statchart_time_series', plugins_url('js/front/statchart-time-series.js', __FILE__), array('dotstat_statchart'), true, true);

	wp_enqueue_style( 'dotstat_bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css');
	wp_enqueue_style( 'front_styles', plugins_url('css/front/dotstat_front_styles.css', __FILE__) );
	
	wp_enqueue_script( 'dotstat_tcp_exporting', plugins_url('js/front/exporting.js', __FILE__), array('dotstat_tcp_highchart'), false, true );
	wp_enqueue_script( 'dotstat_tcp_export_data', plugins_url('js/front/export-data.js', __FILE__), array('dotstat_tcp_exporting'), false, true );

	wp_enqueue_script( 'dotstat_tcp_lollipop', plugins_url('js/front/lollipop.js', __FILE__), array('dotstat_tcp_highcharts-more'), false, true );
}

// Render Gutenberg block
function gutenberg_stst_dotstat_block() {
 
  // automatically load dependencies and version   

  wp_register_script(
    'stst-gutenberg-dotstat-display',
    plugins_url( 'js/admin/dotstat_gutenberg_block.js', __FILE__ ),
    array('wp-blocks', 'wp-editor'),
    '1.1.1'
	);


  register_block_type( 'dostat-gutenberg/dostat-post-gutenberg', array(
		'editor_script' => 'stst-gutenberg-dotstat-display',
		'render_callback' => 'gutenberg_stst_dotstat_block_render'
	) );
	
 
}
add_action( 'init', 'gutenberg_stst_dotstat_block' );


function gutenberg_stst_dotstat_block_render( $block_attributes, $content ) {

	$dotstatId = isset($block_attributes['dotstat']) ? intval($block_attributes['dotstat']) : 0 ;
	
  return '<div>[dotstat display="'.$dotstatId.'"]</div>';
}
