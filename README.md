# DotStat Block plugin

SPC Wordpress plugin to harvest data from Pacific data hub .Stat portal and display it as tables or charts using custom blocks.

## How to

Tutorial post available [here](https://nso.spc.int/blog/2022/07/07/how-to-add-dynamic-charts-to-your-wordpress-pages/).

## Information & Requirements

* Version: 1.2.4
* Requires at least: 5.4
* Tested up to: 5.6.1
* Requires PHP: 7.0


## Changelog
* 1.2.4
  * Use datetime type for time-series chart type (needs a `date` field configured in the .Stat Dataset)
* 1.2.3
  * Fix to new SDMX/JSON syntax
* 1.2.2
  * Display: Upgrade Highcharts to 10.1.0
  * Dataset: Change .stat requests
  * Dataset: Add a JSON viewer
* 1.2.1
  * Display: Keystat now uses Wordpress Dashicons
* 1.2.0
  * Display: Keystat styling
  * Display: Keystat dataset indicator selector
  * Dataset: Adding filter option
  * Dataset: Attributes selection fix

* 1.1.0
  * Code review and changes by SPC

* 1.0.0
  * Initial development by LinkDigital
