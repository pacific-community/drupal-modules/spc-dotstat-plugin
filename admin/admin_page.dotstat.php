<?php

class RegisterDotstatAdminPageNav
{
  /**
  * Holds the values to be used in the fields callbacks
  */
  private $options;

  /**
  * Start up
  */
  public function __construct()
  {
    add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    add_action( 'admin_init', array( $this, 'page_init' ) );
  }

  /**
  * Add options page
  */
  public function add_plugin_page()
  {
    // This page will be under "Settings"
    // add_menu_page(
    //     '.STAT chart Settings', 
    //     '.STAT chart Settings', 
    //     'manage_options', 
    //     'stst_chart_settings', 
    //     array( $this, 'create_admin_page' ),
    //     null,
    //     9.1
    // );
    add_submenu_page(
        'options-general.php',
        '.STAT chart Settings',
        '.STAT chart Settings',
        'manage_options',
        'stst_chart_settings',
        array( $this, 'create_admin_page' ),
    );
  }

  /**
  * Options page callback
  */
  public function create_admin_page()
  {
    // Set class property
    $this->options = get_option( 'stat_chart_option_data' );
    ?>
    <div class="wrap stat_chart_option_wraper">
        <h1>.STAT Charts Settings</h1>
        <form method="post" action="options.php">
        <?php
            // This prints out all hidden setting fields
            settings_fields( 'stat_chart_settings' );
            do_settings_sections( 'stat_chart-setting-admin' );
            submit_button();
        ?>
        </form>
    </div>
    <?php
  }

  /**
  * Register and add settings
  */
  public function page_init()
  {        
    register_setting(
        'stat_chart_settings', // Option group
        'stat_chart_option_data', // Option name
        array( $this, 'sanitize' ) // Sanitize
    );

    add_settings_section(
        'stat_chart_section_id', // ID
        'Chart Settings', // Title
        array( $this, 'print_section_info' ), // Callback
        'stat_chart-setting-admin' // Page
    );  

    add_settings_field(
        'column_chart', // ID
        'Column Chart Settings', // Title 
        array( $this, 'column_chart_callback' ), // Callback
        'stat_chart-setting-admin', // Page
        'stat_chart_section_id' // Section           
    );      

    add_settings_field(
        'drilldown_chart', 
        'Drilldown chart', 
        array( $this, 'drilldown_chart_callback' ), 
        'stat_chart-setting-admin', 
        'stat_chart_section_id'
    );
    
    add_settings_field(
        'lollipop_chart', 
        'Lollipop chart', 
        array( $this, 'lollipop_chart_callback' ), 
        'stat_chart-setting-admin', 
        'stat_chart_section_id'
    );


    add_settings_field(
        'pie_chart', 
        'Pie chart', 
        array( $this, 'pie_chart_callback' ), 
        'stat_chart-setting-admin', 
        'stat_chart_section_id'
    );

    add_settings_field(
        'piesummary_chart', // ID
        'Piesummary Chart', // Title 
        array( $this, 'piesummary_chart_callback' ), // Callback
        'stat_chart-setting-admin', // Page
        'stat_chart_section_id' // Section           
    );  
    

    add_settings_field(
      'populationpyramid_chart', // ID
      'Population Pyramid Chart', // Title 
      array( $this, 'populationpyramid_callback' ), // Callback
      'stat_chart-setting-admin', // Page
      'stat_chart_section_id' // Section           
    ); 

    add_settings_field(
      'populationpyramidmulti_chart', // ID
      'Population Pyramid Multi Chart', // Title 
      array( $this, 'populationpyramidmulti_callback' ), // Callback
      'stat_chart-setting-admin', // Page
      'stat_chart_section_id' // Section           
    ); 

    add_settings_field(
      'population_pyramid_race_chart', // ID
      'Population Pyramid Race Chart', // Title 
      array( $this, 'population_pyramid_race_callback' ), // Callback
      'stat_chart-setting-admin', // Page
      'stat_chart_section_id' // Section           
    ); 

    add_settings_field(
      'timeseries_chart', // ID
      'Time Series Chart', // Title 
      array( $this, 'timeseries_chart_callback' ), // Callback
      'stat_chart-setting-admin', // Page
      'stat_chart_section_id' // Section           
    );

  }

  /**
  * Sanitize each setting field as needed
  *
  * @param array $input Contains all settings fields as array keys
  */
  public function sanitize( $input )
  {
    $new_input = array();
    if( isset( $input['column_chart'] ) )
        $new_input['column_chart'] =  $input['column_chart'];

    if( isset( $input['drilldown_chart'] ) )
        $new_input['drilldown_chart'] =  $input['drilldown_chart'] ;

    if( isset( $input['lollipop_chart'] ) )
        $new_input['lollipop_chart'] =  $input['lollipop_chart'] ;
    
    if( isset( $input['pie_chart'] ) )
        $new_input['pie_chart'] =  $input['pie_chart'] ;

    if( isset( $input['piesummary_chart'] ) )
        $new_input['piesummary_chart'] =  $input['piesummary_chart'] ;

    if( isset( $input['populationpyramid_chart'] ) )
        $new_input['populationpyramid_chart'] =  $input['populationpyramid_chart'] ;

    if( isset( $input['populationpyramidmulti_chart'] ) )
        $new_input['populationpyramidmulti_chart'] =  $input['populationpyramidmulti_chart'] ;

    if( isset( $input['timeseries_chart'] ) )
        $new_input['timeseries_chart'] =  $input['timeseries_chart'] ;

    if( isset( $input['population_pyramid_race_chart'] ) )
        $new_input['population_pyramid_race_chart'] =  $input['population_pyramid_race_chart'] ;

    return $new_input;
  }

  /** 
  * Print the Section text
  */
  public function print_section_info()
  {
    print 'Enter your settings below:';
  }

  /** 
  * Get the settings option array and print one of its values
  */
  public function column_chart_callback()
  {
    printf(
        '<textarea id="column_chart" cols="80" rows="8" name="stat_chart_option_data[column_chart]" >%s</textarea>',
        isset( $this->options['column_chart'] ) && trim( $this->options['column_chart'] )  ?  $this->options['column_chart'] : $this->column_settings()
    );
  }

  /** 
  * Get the settings option array and print one of its values
  */
  public function drilldown_chart_callback()
  {
    printf(
        '<textarea id="drilldown_chart" cols="80" rows="8" name="stat_chart_option_data[drilldown_chart]">%s</textarea>',
        isset( $this->options['drilldown_chart'] ) && trim( $this->options['drilldown_chart'] ) ? $this->options['drilldown_chart'] : $this->drilldown_settings()
    );
  }

  /**
  * 
  */
  public function lollipop_chart_callback()
  {
    printf(
        '<textarea id="lollipop_chart" cols="80" rows="8" name="stat_chart_option_data[lollipop_chart]">%s</textarea>',
        isset( $this->options['lollipop_chart'] ) && trim( $this->options['lollipop_chart'] ) ? $this->options['lollipop_chart'] : $this->lollipop_settings()
    );

  }

  /**
  * 
  */
  public function piesummary_chart_callback()
  {
    printf(
        '<textarea id="piesummary_chart" cols="80" rows="8" name="stat_chart_option_data[piesummary_chart]">%s</textarea>',
        isset( $this->options['piesummary_chart'] ) && trim( $this->options['piesummary_chart'] ) ? $this->options['piesummary_chart'] : $this->piesummary_settings()
    );

  }

  /**
  * 
  */
  public function pie_chart_callback()
  {
    printf(
        '<textarea id="pie_chart" cols="80" rows="8" name="stat_chart_option_data[pie_chart]">%s</textarea>',
        isset( $this->options['pie_chart'] ) && trim( $this->options['pie_chart'] ) ? $this->options['pie_chart'] : $this->pie_settings()
    );

  }

  /**
  * 
  */
  public function populationpyramid_callback()
  {
   printf(
       '<textarea id="populationpyramid_chart" cols="80" rows="8" name="stat_chart_option_data[populationpyramid_chart]">%s</textarea>',
       isset( $this->options['populationpyramid_chart'] ) && trim( $this->options['populationpyramid_chart'] ) ? $this->options['populationpyramid_chart'] : $this->populationpyramid_settings()
   );

  }

  /**
  * 
  */
  public function populationpyramidmulti_callback()
  {
    printf(
        '<textarea id="populationpyramidmulti_chart" cols="80" rows="8" name="stat_chart_option_data[populationpyramidmulti_chart]">%s</textarea>',
        isset( $this->options['populationpyramidmulti_chart'] ) && trim( $this->options['populationpyramidmulti_chart'] ) ? $this->options['populationpyramidmulti_chart'] : $this->populationpyramidmulti_settings()
    );

  }

  /**
  * 
  */
  public function population_pyramid_race_callback()
  {
    printf(
        '<textarea id="population_pyramid_race_chart" cols="80" rows="8" name="stat_chart_option_data[population_pyramid_race_chart]">%s</textarea>',
        isset( $this->options['population_pyramid_race_chart'] ) && trim( $this->options['population_pyramid_race_chart'] ) ? $this->options['population_pyramid_race_chart'] : $this->population_pyramid_race_settings()
    );
  }

  /**
  * 
  */
  public function timeseries_chart_callback()
  {
   printf(
       '<textarea id="timeseries_chart" cols="80" rows="8" name="stat_chart_option_data[timeseries_chart]">%s</textarea>',
       isset( $this->options['timeseries_chart'] ) && trim( $this->options['timeseries_chart'] ) ? $this->options['timeseries_chart'] : $this->timeseries_settings()
   );

  }


  // --------------- CHART SETTINGS


  public function column_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/column.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "column" },
        dotStatChartOptions: {
          rawDataType: "column"
        }
      }';
    } 
  }

  public function drilldown_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/drilldown.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "column" },
        dotStatChartOptions: {
          rawDataType: "drilldown"
        }
      }';
    }
  }


  public function lollipop_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/lollipop.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "lollipop" },
        dotStatChartOptions: {
          rawDataType: "lollipop"
        }
      }';
    }   
  }

  public function piesummary_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/pie_summary.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "pie" },
        dotStatChartOptions: {
          rawDataType: "piesummary"
        }
      }';
    }
  }

  public function pie_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/pie.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "pie" },
        dotStatChartOptions: {
          rawDataType: "pie"
        }
      }';
    }
  }

  public function populationpyramid_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/population_pyramid.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "bar" },
        dotStatChartOptions: {
          rawDataType: "population_pyramid"
        }
      }';
    }
  }

  public function populationpyramidmulti_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/population_pyramid_multi.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "bar" },
        dotStatChartOptions: {
          rawDataType: "population_pyramid_multi"
        }
      }';
    }
  }

  public function population_pyramid_race_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/population_pyramid_race.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "bar" },
        dotStatChartOptions: {
          rawDataType: "population_pyramid_race"
        }
      }';
    }
  }

  public function timeseries_settings(){
    $path = plugin_dir_path( __DIR__ ).'config/time_series.json';
    if (file_exists($path)) {     
      return file_get_contents($path);
    } else {
      return '
      { 
        chart: { type: "line" },
        dotStatChartOptions: {
          rawDataType: "time_series"
        }
      }';
    }
  }

  
} // end class
    
if( is_admin() )
    $my_settings_page = new RegisterDotstatAdminPageNav();